clear
close all
fclose('all');
%%% basic informations
tic;

% Add a directory for LTM functions
addpath('Functions');

% Output the results in './Output/result.log'
fileID = fopen("Output\result.log", 'w');
fprintf(fileID, "Line Tension Model ver. 1.0 by CMML\n");
fprintf(fileID, "Computed on %s\n", datestr(now));

% Store data for plotting in txt files
fileID_figure1 = fopen("Output\figure1.txt", 'w');
fileID_figure2 = fopen("Output\figure2.txt", 'w');
fileID_figure3 = fopen("Output\figure3.txt", 'w');


%% sub
W = 23;
G = 25.6;
H = 1000;
SUB_INFO = [W,G,H];

num_pillar = 100;
grav = 1e-15 * 9.8e6/72.6e-3;

SUB_coord= zeros(4*2*num_pillar+4, 2);
for k = -num_pillar:num_pillar
    SUB_coord(4*(k+num_pillar)+1,1) = 0 + (k-1)*(W+G);
    SUB_coord(4*(k+num_pillar)+1,2) = -H;
    SUB_coord(4*(k+num_pillar)+2,1) = 0 + (k-1)*(W+G);
    SUB_coord(4*(k+num_pillar)+2,2) = 0;
    SUB_coord(4*(k+num_pillar)+3,1) = W + (k-1)*(W+G);
    SUB_coord(4*(k+num_pillar)+3,2) = 0;
    SUB_coord(4*(k+num_pillar)+4,1) = W + (k-1)*(W+G);
    SUB_coord(4*(k+num_pillar)+4,2) = -H;
end

fprintf(fileID_figure1, "%s\n", "The X Coordinates of nodes of substrates follow in next line");
fprintf(fileID_figure1, "%10.5f  ", SUB_coord(:,1));
fprintf(fileID_figure1, "\n%s\n", "The Y Coordinates of nodes of substrates follow in next line");
fprintf(fileID_figure1, "%10.5f  ", SUB_coord(:,2));
%figure(45); hold on
%plot(SUB_coord(:,1) ,SUB_coord(:,2));

fprintf(fileID_figure2, "%s\n", "The X Coordinates of nodes of substrates follow in next line");
fprintf(fileID_figure2, "%10.5f  ", SUB_coord(:,1));
fprintf(fileID_figure2, "\n%s\n", "The Y Coordinates of nodes of substrates follow in next line");
fprintf(fileID_figure2, "%10.5f  ", SUB_coord(:,2));
%figure(55); hold on
%plot(SUB_coord(:,1) ,SUB_coord(:,2));

fprintf(fileID_figure3, "%s\n", "The X Coordinates of nodes of substrates follow in next line");
fprintf(fileID_figure3, "%10.5f  ", SUB_coord(:,1));
fprintf(fileID_figure3, "\n%s\n", "The Y Coordinates of nodes of substrates follow in next line");
fprintf(fileID_figure3, "%10.5f  ", SUB_coord(:,2));
%figure(66); hold on
%plot(SUB_coord(:,1) ,SUB_coord(:,2));
%drawnow;

%% gamma_position

theta_e = 95*pi/180;
contact_line_thickness = 3;
alpha = 6/contact_line_thickness;

gamma_1 = 1;% gamma_LV
gamma_2 = -cos(theta_e)*gamma_1; %gamma_SL-gamma_SV
rho = 1000;

% figure (77);
xCC = -1:0.01:2 * contact_line_thickness;
gammaCC = (1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)*tanh(alpha*(xCC-contact_line_thickness)));

% plot(xCC,gammaCC);
% drawnow;

%% initial cal E ,G
L = 1*W+0*G;
Vol0 = 5e6;

%%% initial shape of the droplet with L,Vol0
N_node_init = 100;
[theta_i, R_i, res_i] = find_theta(L,sqrt(Vol0/pi), 50, 0.00001);
theta_init_data = [-theta_i + pi/2:2*theta_i/(N_node_init-1):theta_i + pi/2]';

CB_initial_A = [[-L/2:L/19:L/2] + W/2 ; zeros(1,20) + 0.5*contact_line_thickness]';
CB_initial_B = [R_i*cos(theta_init_data)+ W/2,R_i*sin(theta_init_data)-R_i*cos(theta_i) + 0.5*contact_line_thickness] ;
CB_initial = [CB_initial_A;CB_initial_B];

fprintf(fileID_figure2, "\n%s\n", "The X Coordinates of nodes of condensed body follow in next line (CB_initial(:,1))");
fprintf(fileID_figure2, "%10.5f  ", CB_initial(:,1));
fprintf(fileID_figure2, "\n%s\n", "The Y Coordinates of nodes of condensed body follow in next line (CB_initial(:,2))");
fprintf(fileID_figure2, "%10.5f  ", CB_initial(:,2));
%figure (55);
%plot([CB_initial(:,1);CB_initial(1,1)],[CB_initial(:,2);CB_initial(1,2)],'*-');hold on

fprintf(fileID_figure3, "\n%s\n", "The X Coordinates of nodes of condensed body follow in next line (CB_initial(:,1))");
fprintf(fileID_figure3, "%10.5f  ", CB_initial(:,1));
fprintf(fileID_figure3, "\n%s\n", "The Y Coordinates of nodes of condensed body follow in next line (CB_initial(:,2))");
fprintf(fileID_figure3, "%10.5f  ", CB_initial(:,2));
%figure (66);
%plot([CB_initial(:,1);CB_initial(1,1)],[CB_initial(:,2);CB_initial(1,2)],'*-');hold on

CB_coord = CB_initial;
acc = 1e-4 ;                                                 % Accuracy in sum(gradient.^2)
maxfn = 2000;                                              % Max # of fcn evaluation
dfpred = 1*sqrt(Vol0);  

% assign augumented lagrange method parameters
lambda1 = 0;                                                 % Lagrange lambda
rk10 = 0.001/sqrt(Vol0);
% Lagrange penalty constants
lambda2 = 0;                                                 % Lagrange lambda
rk20 = 1/(sqrt(Vol0));
lambda3 = 0;    
rk30 = 1/(sqrt(Vol0));
cb_X0_1array = convert2_1Darray( CB_coord);

[cb_X0_1array,LU] = Merge_split_20(cb_X0_1array,0.5,1.5,alpha,contact_line_thickness,SUB_INFO,1);
[Einitial,Ginitial,gh,gs] = potential_wrapper_pos_newg_anal_show(cb_X0_1array,Vol0,lambda1,rk10,lambda2,lambda3,rk30,rk20,contact_line_thickness,gamma_1,gamma_2,alpha,grav,rho,SUB_INFO,LU);

CB_initial2 = convert2_multiarray(cb_X0_1array);

%figure(55); hold on
fprintf(fileID_figure2, "\n%s\n", "The X Coordinates of nodes of condensed body follow in next line (CB_initial2(:,1))");
fprintf(fileID_figure2, "%10.5f  ", CB_initial2(:,1));
fprintf(fileID_figure2, "\n%s\n", "The Y Coordinates of nodes of condensed body follow in next line (CB_initial2(:,2))");
fprintf(fileID_figure2, "%10.5f  ", CB_initial2(:,2));
%plot([CB_initial2(:,1);CB_initial2(1,1)],[CB_initial2(:,2);CB_initial2(1,2)],'r*-');hold on
CB_Ginitial = convert2_multiarray(Ginitial);
fprintf(fileID_figure2, "\n%s\n", "The X starting point of quiver follow in next line (CB_initial2(:,1))");
fprintf(fileID_figure2, "%10.5f  ", CB_initial2(:,1));
fprintf(fileID_figure2, "\n%s\n", "The Y starting point of quiver follow in next line (CB_initial2(:,2))");
fprintf(fileID_figure2, "%10.5f  ", CB_initial2(:,2));
fprintf(fileID_figure2, "\n%s\n", "The X direction of quiver follow in next line (CB_Ginitial(:,1))");
fprintf(fileID_figure2, "%10.5f  ", CB_Ginitial(:,1));
fprintf(fileID_figure2, "\n%s\n", "The Y direction of quiver follow in next line (CB_Ginitial(:,2))");
fprintf(fileID_figure2, "%10.5f  ", CB_Ginitial(:,2));
%quiver(CB_initial2(:,1),CB_initial2(:,2),CB_Ginitial(:,1),CB_Ginitial(:,2));


%%%%%%%%%%%%%%%
% % 
rk1 = rk10;
rk2 = rk20 ;
rk3 = rk30 ;

zeta1 = 1e-4*Vol0;
zeta2 = 10;
zeta3 = 1;

num_gMax = 3;
is_converged = zeros(1, num_gMax);
fprintf(fileID, "%s\n", "The droplet shape change with gravity. Volume effect or tilt angle can be investigated too. (Vol1 = Vol0*1.01^num_g)");
fprintf(fileID, "%s%d\n", "num_g value changes by 1, from 1 to ", num_gMax);

for num_g = 1:num_gMax
    %%%     droplet shape change about gravity. Volume effect or tilt angle can be
    %%%     investigated too. (Vol1 = Vol0*1.01^num_g)
    fprintf(fileID, "\n%s%d\n", "num_g is ", num_g);
    grav1 = min(grav/10 * (num_g-1),grav);
    CB_temp = convert2_multiarray(cb_X0_1array);
    % CB_temp(:,2) = max(CB_temp(:,2),0.1*contact_line_thickness);
    cb_X0_1array = convert2_1Darray(CB_temp);

    maxIter = 200;
    for kkp = 1:maxIter
        if kkp == maxIter
            fprintf(fileID, "%s\n", '[WARNING]: NOT CONVERGED');
            % is_converged(num_g) = 0; % it's initialized as 0
            break
        end

    [Etotf,gtotf,cb_Xf_1array,gh,gs] = cgrelax_pos2_show('potential_wrapper_pos_newg_anal_show',acc,maxfn,dfpred,cb_X0_1array,Vol0, lambda1 ,rk1,lambda2, rk2,lambda3,rk3,contact_line_thickness,gamma_1,gamma_2,alpha,grav1,rho,SUB_INFO,LU, fileID, fileID_figure1);
    
    %figure(66); hold on
    CB_coord_temp5 = convert2_multiarray(cb_Xf_1array);
    G_temp5 = convert2_multiarray(gtotf);
    Gh_temp5 = convert2_multiarray(gh);
    Gs_temp5 = convert2_multiarray(gs);
    fprintf(fileID_figure3, "\n%s\n", "The X Coordinates of nodes of condensed body follow in next line (CB_coord_temp5(:,1))");
    fprintf(fileID_figure3, "%10.5f  ", CB_coord_temp5(:,1));
    fprintf(fileID_figure3, "\n%s\n", "The Y Coordinates of nodes of condensed body follow in next line (CB_coord_temp5(:,2))");
    fprintf(fileID_figure3, "%10.5f  ", CB_coord_temp5(:,2));
    %plot([CB_coord_temp5(:,1);CB_coord_temp5(1,1)],[CB_coord_temp5(:,2);CB_coord_temp5(1,2)],'*-');hold on
    
    fprintf(fileID_figure3, "\n%s\n", "The X starting point of quiver follow in next line (CB_coord_temp5(:,1))");
    fprintf(fileID_figure3, "%10.5f  ", CB_coord_temp5(:,1));
    fprintf(fileID_figure3, "\n%s\n", "The Y starting point of quiver follow in next line (CB_coord_temp5(:,2))");
    fprintf(fileID_figure3, "%10.5f  ", CB_coord_temp5(:,2));
    fprintf(fileID_figure3, "\n%s\n", "The X direction of quiver follow in next line (G_temp5(:,1))");
    fprintf(fileID_figure3, "%10.5f  ", G_temp5(:,1));
    fprintf(fileID_figure3, "\n%s\n", "The Y direction of quiver follow in next line (G_temp5(:,2))");
    fprintf(fileID_figure3, "%10.5f  ", G_temp5(:,2));
    %quiver(CB_coord_temp5(:,1),CB_coord_temp5(:,2),G_temp5(:,1),G_temp5(:,2));
    
    fprintf(fileID_figure3, "\n%s\n", "The X direction of quiver follow in next line (Gh_temp5(:,1))");
    fprintf(fileID_figure3, "%10.5f  ", Gh_temp5(:,1));
    fprintf(fileID_figure3, "\n%s\n", "The Y direction of quiver follow in next line (Gh_temp5(:,2))");
    fprintf(fileID_figure3, "%10.5f  ", Gh_temp5(:,2));
    %quiver(CB_coord_temp5(:,1),CB_coord_temp5(:,2),Gh_temp5(:,1),Gh_temp5(:,2));
    
    fprintf(fileID_figure3, "\n%s\n", "The X direction of quiver follow in next line (Gs_temp5(:,1))");
    fprintf(fileID_figure3, "%10.5f  ", Gs_temp5(:,1));
    fprintf(fileID_figure3, "\n%s\n", "The Y direction of quiver follow in next line (Gs_temp5(:,2))");
    fprintf(fileID_figure3, "%10.5f  ", Gs_temp5(:,2));
    %quiver(CB_coord_temp5(:,1),CB_coord_temp5(:,2),Gs_temp5(:,1),Gs_temp5(:,2));

    CB_coord_temp = convert2_multiarray(cb_Xf_1array);
    Vol = calc_volume(CB_coord_temp);
    h1 = (Vol - Vol0);
    h2 = calc_h2_one(CB_coord_temp,SUB_INFO);
    h3 = calc_h3_two(CB_coord_temp,alpha,contact_line_thickness,SUB_INFO,LU);
    h_length = norm([h1 h2 h3]);
    is_constrained = [h1/zeta1 h2/zeta2 h3/zeta3];


    if abs(h1)<zeta1 && abs(h2)<zeta2 && abs(h3)<zeta3
        if (norm(gtotf))^2 < acc %&& kkp>50
            rk_DATA = [rk1 rk2 rk3];
            lambda_DATA = [lambda1 lambda2 lambda3];
            zeta_DATA = [zeta1 zeta2 zeta3];
            is_converged(num_g) = 1;
            fprintf(fileID, "%s\n", "It's converged for this num_g value");
            fname = sprintf("Output\\CB_info_adv_%d_10g.mat",num_g-1);
            save(fname,'CB_coord_temp','rk_DATA','lambda_DATA','zeta_DATA');
            break;
        end
    end
        
    
    if abs(h1)<zeta1 && abs(h1) > 0.1*zeta1
        lambda_new1 = lambda1- rk1*h1;
        lambda1 = lambda_new1;
    elseif abs(h1)>=zeta1
        rk1 = rk1*2;
    else
        rk1 = rk1/2;
    end
    
    
    if abs(h2)<zeta2 && abs(h2) > 0.1*zeta2
        lambda_new2 = lambda2- rk2*h2;
        lambda2 = lambda_new2;
   elseif abs(h2)>=zeta2
        rk2 = rk2*2;
    else
        rk2 = rk2/2;
    end
    
        
    if abs(h3)<zeta3 && abs(h3) > 0.1*zeta3
        lambda_new3 = lambda3- rk3*h3;
        lambda3 = lambda_new3;
   elseif abs(h3)>=zeta3
        rk3 = rk3*2;
    else
        rk3 = rk3/2;
    end
    
    
    [cb_Xf_1array,LU_ms_ed] = Merge_split_20(cb_Xf_1array,0.5,1.5,alpha,contact_line_thickness,SUB_INFO,kkp);
    CB_coord_temp = convert2_multiarray(cb_Xf_1array);
    G_temp = convert2_multiarray(gtotf);
    
    fprintf(fileID_figure2, "\n%s\n", "The X Coordinates of nodes of condensed body follow in next line (CB_coord_temp(:,1))");
    fprintf(fileID_figure2, "%10.5f  ", CB_coord_temp(:,1));
    fprintf(fileID_figure2, "\n%s\n", "The Y Coordinates of nodes of condensed body follow in next line (CB_coord_temp(:,2))");
    fprintf(fileID_figure2, "%10.5f  ", CB_coord_temp(:,2));
    %figure(55); hold on
    %plot([CB_coord_temp(:,1);CB_coord_temp(1,1)],[CB_coord_temp(:,2);CB_coord_temp(1,2)],'*-');hold on
    %drawnow;

    cb_X0_1array =  cb_Xf_1array;
    LU = LU_ms_ed;

    end

% figure(171); hold off
% plot([CB_coord_temp(:,1);CB_coord_temp(1,1)],[CB_coord_temp(:,2);CB_coord_temp(1,2)],'o-');

end
%is_converged;
compTime = toc;
fprintf(fileID, "The whole computation is done and the elapsed time is %f sec", compTime);
fclose(fileID);
% 