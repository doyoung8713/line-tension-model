function [ G_tot_sum_anal] = calc_grad_anal_period( x1,x2,y1,y2,gamma_1,gamma_2,alpha,contact_line_thickness,SUB_INFO)
%UNTITLED2 이 함수의 요약 설명 위치
%   자세한 설명 위치
    W = SUB_INFO(1);
    G = SUB_INFO(2);
    H = SUB_INFO(3);
    [ periods,points,gshape ] = find_period( x1,x2,y1,y2,SUB_INFO );


    num_region = size(periods,2);

    for k = 1:num_region;
       
        Xa = points(k,1)-periods(k)*(W+G);
        Xb = points(k+1,1)-periods(k)*(W+G);
        Ya = points(k,2);
        Yb = points(k+1,2);
        [Xa Xb Ya Yb];
        gshape_x1 = [gshape(k,1) gshape(k+1,1) gshape(k,5) gshape(k+1,5)]';
        gshape_x2 = [gshape(k,2) gshape(k+1,2) gshape(k,6) gshape(k+1,6)]';
        gshape_y1 = [gshape(k,3) gshape(k+1,3) gshape(k,7) gshape(k+1,7)]';
        gshape_y2 = [gshape(k,4) gshape(k+1,4) gshape(k,8) gshape(k+1,8)]';
    


        G_anal = calc_grad_anal_sum( Xa,Xb,Ya,Yb,gamma_1,gamma_2,alpha,contact_line_thickness,SUB_INFO);
%         G_anal = calc_grad_anal_sum( 22.99,26.99,Ya,Yb,gamma_1,gamma_2,alpha,contact_line_thickness,SUB_INFO)

        G_tot_anal(k,:) = [G_anal * gshape_x1 G_anal * gshape_x2  G_anal * gshape_y1  G_anal * gshape_y2];
%         G_tot_anal(k,:) = G_anal;
        
    end

        G_tot_sum_anal = sum(G_tot_anal,1);







    end


