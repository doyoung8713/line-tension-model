function [f,g,x,gh,gs] = cgrelax_pos2_show(func,acc,maxfn,dfpred,X_cb,Vol0, lambda1 ,rk1,lambda2, rk2,lambda3,rk3,contact_line_thickness,gamma_1,gamma_2,alpha,grav,rho,SUB_INFO,LU, fileID, fileID_figure1)
%% Stand alone conjugate gradient relaxiation program
%%
%%    Based on IMSL's U2CGG.F
%%
%%  rewriten in C by Dongyi Liao at MIT 1999
%%  rewriten in matlab by Wei Cai at LLNL 2004
%%
%%  modified in matlab by Doyoung Kim at CMML 2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NDIM = size(X_cb,1);

% define
MAXLIN = 10; %% Maximum iterations within a line search
MXFCON = 5; %% Maximum iterations before F can be decreased

%initialize memory
rss=zeros(NDIM,1);
rsg=zeros(NDIM,1);
ginit=zeros(NDIM,1);

iterPrint = zeros(1, maxfn);
fPrint = zeros(1, maxfn);
gsumPrint = zeros(1, maxfn);

ddspln=0;
gamden=0;

ITERC=0;
iterfm=0;
iterrs=0;
x=X_cb;
[f,g,gh,gs]=feval(func,x,Vol0,lambda1,rk1,lambda2,rk2,lambda3,rk3,contact_line_thickness,gamma_1,gamma_2,alpha,grav,rho,SUB_INFO,LU);
ncalls=1;                                       % first calculation
fprintf(fileID, "%s\n", 'relax: 1st potential call finished.');

s=-g;
gsum=sum(g.*g);     % Sum of gradient^2

if(gsum<=acc)
    fprintf(fileID, "%s\n", 'converged');
    return;
end

fmin=f;     % Output energy from the function in 1st evaluation
gsqrd=gsum; % Sum of gradient^2
nfopt=ncalls;

xopt=x;     % Input coordinate in 1D array
gopt=g;     % Output gradient from the function in 1st evaluation

dfpr=dfpred; % Input parameter from Main
stmin=dfpred/gsqrd;

reinsert=0;
iter_count = 1;
while(1)
    %% BEGIN THE ITERATION
    
    if(~reinsert) %% A MECHANISM TO IMPLEMENT RETRY
        indexPrint = 0;
        ITERC = ITERC+1;
        
        %iterPrint(indexPrint) = ITERC;

        %% STORE THE INITIAL FUNCTION VALUE AND GRADIENT, CALCULATE THE
        %% INITIAL DIRECTIONAL DERIVATIVE, AND BRANCH IF ITS VALUE IS NOT
        %% NEGATIVE. SET SBOUND TO MINUS ONE TO INDICATE THAT A BOUND ON THE
        %% STEP IS NOT KNOWN YET, AND SET NFBEG TO THE CURRENT VALUE OF
        %% NCALLS. THE PARAMETER IRETRY SHOWS THE NUMBER OF ATTEMPTS AT
        %% SATISFYING THE BETA CONDITION.
        finit=f;
        ginit=g;
        
        gfirst=sum(s.*g); % -1.84e+20
        if(gfirst >=0)
            %% SET IER TO INDICATE THAT THE SEARCH DIRECTION IS UPHILL.
            fprintf(fileID, "%s\n", 'ERROR [cgrelax]: Search direction is uphill.');
            return;
        end
        gmin=gfirst;
        sbound=-1;
        nfbeg= ncalls;
        iretry=-1;
        %% SET STEPCH SO THAT THE INITIAL STEP-LENGTH IS CONSISTENT WITH THE
        %% PREDICTED REDUCTION IN F, SUBJECT TO THE CONDITION THAT IT DOES
        %% NOT EXCEED THE STEP-LENGTH OF THE PREVIOUS ITERATION. LET STMIN
        %% BE THE STEP TO THE LEAST CALCULATED VALUE OF F.
        stepch=min(stmin, abs(dfpr/gfirst));
        stmin=0;
        %% CALL SUBROUTINE FUNCT AT THE VALUE OF X THAT IS DEFINED BY THE
        %% NEW CHANGE TO THE STEP-LENGTH, AND LET THE NEW STEP-LENGTH BE
        %% STEP. THE VARIABLE WORK IS USED AS WORK SPACE.
        
    end %% A MECHANISM TO IMPLEMENT RETRY
    while(1)
        if(~reinsert) %% A MECHANISM TO IMPLEMENT RETRY
            indexPrint = indexPrint + 1;
            
            step=stmin+stepch;
            x=xopt+stepch*s;
            temp=max(abs(x-xopt));
            
            if(temp <= 0)
                %% TERMINATE THE LINE SEARCH IF STEPCH IS EFFECTIVELY ZERO.
                if(ncalls > nfbeg+1 || abs(gmin/gfirst) > 0.2)
                    
                    fprintf(fileID, "%s\n", 'ERROR [cgrelax]: Line search aborted, possible error in gradient2.');
                    return;
                else
                    break;
                end
            end
            
            ncalls = ncalls+1;
            [f,g,gh,gs] = feval(func,x,Vol0,lambda1,rk1,lambda2,rk2,lambda3,rk3,contact_line_thickness,gamma_1,gamma_2,alpha,grav,rho,SUB_INFO,LU);
            fPrint(indexPrint) = f;
            
            %% SET SUM TO G SQUARED. GMIN AND GNEW ARE THE OLD AND THE NEW
            %% DIRECTIONAL DERIVATIVES ALONG THE CURRENT SEARCH DIRECTION.
            gnew=sum(s.*g);
            gsum=sum(g.*g);
            gsumPrint(indexPrint) = gsum;
            %% STORE THE VALUES OF X, F AND G, IF THEY ARE THE BEST THAT
            %% HAVE BEEN CALCULATED SO FAR, AND NOTE G SQUARED AND THE VALUE
            %% OF NCALLS. TEST FOR CONVERGENCE.
            if((f < fmin || (f==fmin && gnew/gmin>=-1)))
                fmin=f;
                gsqrd=gsum;
                nfopt=ncalls;
                xopt=x;
                gopt=g;
            end
            
            fprintf(fileID, "%10d %20.10f %20.10f\n",ITERC, f, gsum);
            if(f <= fmin && gsum <= acc)
                
                for i = 1:indexPrint
                    %fprintf(fileID, "%5d %15.10f %15.10f\n", i, fPrint(i), gsumPrint(i));
                end
                
                fprintf(fileID, "%s\n", "It's converged. Finish cgrelax.");
                return;
            end
            
            if(ncalls==maxfn)
                fprintf(fileID, "%s\n", 'ERROR [cgrelax]: Too many iterations, stop...');
                return;
            end
            
            %% LET SPLN BE THE QUADRATIC SPLINE THAT INTERPOLATES THE
            %% CALCULATED FUNCTION VALUES AND DIRECTIONAL DERIVATIVES AT THE
            %% POINTS STMIN AND STEP OF THE LINE SEARCH, WHERE THE KNOT OF
            %% THE SPLINE IS AT 0.5*(STMIN+STEP). REVISE STMIN, GMIN AND
            %% SBOUND, AND SET DDSPLN TO THE SECOND DERIVATIVE OF SPLN AT
            %% THE NEW STMIN. HOWEVER, IF FCH IS ZERO, IT IS ASSUMED THAT
            %% THE MAXIMUM ACCURACY IS ALMOST ACHIEVED, SO DDSPLN IS
            %% CALCULATED USING ONLY THE CHANGE IN THE GRADIENT.
            temp=2*(f-fmin)/stepch-gnew-gmin;
            ddspln=(gnew-gmin)/stepch;
           if(ncalls > nfopt)
               sbound=step;
           else
              if(gmin*gnew <= 0)
                  sbound=stmin;
              end
           stmin=step;
           gmin=gnew;
           stepch=-stepch;
           end
            
           
            if(f~=fmin)
                ddspln=ddspln+(temp+temp)/stepch;
            end
            
            %% TEST FOR CONVERGENCE OF THE LINE SEARCH, BUT FORCE AT LEAST
            %% TWO STEPS TO BE TAKEN IN ORDER NOT TO LOSE QUADRATIC
            %% TERMINATION.
            if(gmin==0)
                break;
            end
            if(ncalls >= nfbeg+1)
                if(abs(gmin/gfirst) <=0.2)
                    break;
                end
                %% APPLY THE TEST THAT DEPENDS ON THE PARAMETER MAXLIN.
                %% l_retry:
                if(ncalls >= nfopt+MAXLIN)
                    fprintf(fileID, "%s\n", 'ERROR [cgrelax]: Line search aborted, possible error in gradient1.');
                    return;
                end
            end
            
        end %% A MECHANISM TO IMPLEMENT RETRY
        
        if(reinsert)%% A MECHANISM TO IMPLEMENT RETRY
            reinsert=0;
            if(ncalls >= nfopt+MAXLIN)
                fprintf(fileID, "%s\n", 'ERROR [cgrelax]: Line search aborted, possible error in gradient3.');
                return;
            end
        end %% A MECHANISM TO IMPLEMENT RETRY
        
        %% SET STEPCH TO THE GREATEST CHANGE TO THE CURRENT VALUE OF STMIN
        %% THAT IS ALLOWED BY THE BOUND ON THE LINE SEARCH. SET GSPLN TO THE
        %% GRADIENT OF THE QUADRATIC SPLINE AT (STMIN+STEPCH). HENCE
        %% CALCULATE THE VALUE OF STEPCH THAT MINIMIZES THE SPLINE FUNCTION,
        %% AND THEN OBTAIN THE NEW FUNCTION AND GRADIENT VECTOR, FOR THIS
        %% VALUE OF THE CHANGE TO THE STEP-LENGTH.
        stepch=0.5*(sbound-stmin);
        if(sbound < -0.5)
            stepch=9*stmin;
        end
        gspln=gmin+stepch*ddspln;
        if(gmin*gspln<0)
            stepch=stepch*gmin/(gmin-gspln);
        end
    end %% END OF INNER WHILE LOOP
    %% ENSURE THAT F, X AND G ARE OPTIMAL.
    if(ncalls~=nfopt)
        f=fmin;
        x=xopt;
        g=gopt;
    end
    %% CALCULATE THE VALUE OF BETA THAT OCCURS IN THE NEW SEARCH
    %% DIRECTION.
    gsum=sum(g.*ginit);
    beta=(gsqrd-gsum)/(gmin-gfirst);
    %% TEST THAT THE NEW SEARCH DIRECTION CAN BE MADE DOWNHILL. IF IT
    %% CANNOT, THEN MAKE ONE ATTEMPT TO IMPROVE THE ACCURACY OF THE LINE
    %% SEARCH.
    if(abs(beta*gmin) > 0.2*gsqrd)
        iretry=iretry+1;
        if(iretry<=0)
            %% goto l_retry;
            %% disp('need to retry: not implemented yet');
            reinsert = 1;
            continue;
        end
    end
    %% APPLY THE TEST THAT DEPENDS ON THE PARAMETER MXFCON.
    %% SET DFPR TO THE PREDICTED REDUCTION IN F ON THE NEXT ITERATION.
    if(f<finit)
        iterfm=ITERC;
    elseif(ITERC >= iterfm+MXFCON)
        fprintf(fileID, "%s\n", 'ERROR [cgrelax]: Cannot reduce value of F, aborting...');
        return;
    end
    
    dfpr=stmin*gfirst;
    
    if(iretry>0) %% Restart since we need to retry
        s=-g;
        iterrs=0;
        continue;
    end
    
    if(iterrs ~= 0 && (ITERC-iterrs) < NDIM && abs(gsum) < 0.2*gsqrd)
        %% CALCULATE THE VALUE OF GAMA THAT OCCURS IN THE NEW SEARCH
        %% DIRECTION, AND SET SUM TO A SCALAR PRODUCT FOR THE TEST BELOW.
        %% THE VALUE OF GAMDEN IS SET BY THE RESTART PROCEDURE.
        %% tmp1=tmp2=tmp3=0;
        gama=sum(g.*rsg);
        gsum=sum(g.*rss);
        gama=gama/gamden;
        %% RESTART IF THE NEW SEARCH DIRECTION IS NOT SUFFICIENTLY
        %% DOWNHILL.
        if(abs(beta*gmin+gama*gsum) < 0.2*gsqrd)
            %% CALCULATE THE NEW SEARCH DIRECTION.
            s=-g+beta*s+gama*rss;
            continue;
        end
    end
    %% APPLY THE RESTART PROCEDURE.
    gamden=gmin-gfirst;
    
    rss=s;
    rsg=g-ginit;
    s=-g+beta*s;
    iterrs=ITERC;
    
    X_CB_plot = convert2_multiarray(x);
    G_CB_plot = convert2_multiarray(g);
   
    if mod(iter_count,20) == 0
    
    %figure(45)
    fprintf(fileID_figure1, "\n%s\n", "The X Coordinates of nodes of condensed body follow in next line (X_CB_plot(:,1))");
    fprintf(fileID_figure1, "%10.5f  ", X_CB_plot(:,1));
    fprintf(fileID_figure1, "\n%s\n", "The Y Coordinates of nodes of condensed body follow in next line (X_CB_plot(:,2))");
    fprintf(fileID_figure1, "%10.5f  ", X_CB_plot(:,2));
    %plot(X_CB_plot(:,1),X_CB_plot(:,2),'o-'); hold on
    %drawnow;
    fprintf(fileID_figure1, "\n%s\n", "The X starting point of quiver follow in next line (X_CB_plot(:,1))");
    fprintf(fileID_figure1, "%10.5f  ", X_CB_plot(:,1));
    fprintf(fileID_figure1, "\n%s\n", "The Y starting point of quiver follow in next line (X_CB_plot(:,2))");
    fprintf(fileID_figure1, "%10.5f  ", X_CB_plot(:,2));
    fprintf(fileID_figure1, "\n%s\n", "The X direction of quiver follow in next line (G_CB_plot(:,1))");
    fprintf(fileID_figure1, "%10.5f  ", G_CB_plot(:,1));
    fprintf(fileID_figure1, "\n%s\n", "The Y direction of quiver follow in next line (G_CB_plot(:,2))");
    fprintf(fileID_figure1, "%10.5f  ", G_CB_plot(:,2));
    %quiver(X_CB_plot(:,1),X_CB_plot(:,2),G_CB_plot(:,1),G_CB_plot(:,2));
    end
     iter_count = iter_count+1;
end %% END OF WHILE LOOP

%% END OF CGRELAX