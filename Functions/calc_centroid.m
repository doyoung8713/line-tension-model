function [ C,GCx,GCy ] = calc_centroid( X )
%UNTITLED 이 함수의 요약 설명 위치
%   자세한 설명 위치

X = [X(end,:); X; X(1,:)];
V = 1/2 * sum((X(2:end-1,1) + X(3:end,1)) .* (X(3:end,2) - X(2:end-1,2)));

dVdx = 1/2 * (X(3:end,2) - X(1:end-2,2));
dVdy = 1/2 * (-X(3:end,1) + X(1:end-2,1));


Fx = sum((X(2:end-1,1)+X(3:end,1)).*(X(2:end-1,1).*X(3:end,2) - X(3:end,1).*X(2:end-1,2)));
Fy = sum((X(2:end-1,2)+X(3:end,2)).*(X(2:end-1,1).*X(3:end,2) - X(3:end,1).*X(2:end-1,2)));

Cx = 1/6/V * Fx;
Cy = 1/6/V * Fy;
C = [Cx Cy];

dFxdx =  X(1:end-2,1).*X(2:end-1,2) - X(2:end-1,1).*X(1:end-2,2) + (X(1:end-2,1)+X(2:end-1,1)).*(-X(1:end-2,2)) + ...
             X(2:end-1,1).*X(3:end,2) - X(3:end,1).*X(2:end-1,2) + (X(2:end-1,1) + X(3:end,1)).*X(3:end,2);
dFxdy = (X(1:end-2,1)+X(2:end-1,1)).*X(1:end-2,1) + (X(2:end-1,1) + X(3:end,1)).*(-X(3:end,1));
         
dFydx = (X(1:end-2,2)+X(2:end-1,2)).*(-X(1:end-2,2)) + (X(2:end-1,2) + X(3:end,2)).*(X(3:end,2));         
dFydy = X(1:end-2,1).*X(2:end-1,2) - X(2:end-1,1).*X(1:end-2,2) + (X(1:end-2,2)+X(2:end-1,2)).*(X(1:end-2,1)) + ...
             X(2:end-1,1).*X(3:end,2) - X(3:end,1).*X(2:end-1,2) + (X(2:end-1,2) + X(3:end,2)).*(-X(3:end,1));  
         
dCxdx = (dFxdx*V - Fx.*dVdx)/6/V^2;
dCxdy = (dFxdy*V - Fx.*dVdy)/6/V^2;
GCx = [dCxdx, dCxdy];

dCydx = (dFydx*V - Fy.*dVdx)/6/V^2;
dCydy = (dFydy*V - Fy.*dVdy)/6/V^2;

GCy = [dCydx, dCydy];



% pert = 1000*sqrt(eps);
% for s = 1:size(X,1);
%     for j= 1:2
%         X_pert_p = X;
%         X_pert_p(s,j) = X_pert_p(s,j) + pert;
%         X_pert_n= X;
%         X_pert_n(s,j) = X_pert_n(s,j) - pert;
%         
%         
%         C_p = calc_centroid(X_pert_p);
%         C_n = calc_centroid(X_pert_n);
%         
%         GCx_N(s,j) = (C_p(1)-C_n(1))/2/pert;
%         GCy_N(s,j) = (C_p(2)-C_n(2))/2/pert;        
%     end
%     
% 
% end

end

