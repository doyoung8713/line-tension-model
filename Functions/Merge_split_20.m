function [ X_cb_out,LU ] = Merge_split_3( X_cb,thresh_small,thresh_large,alpha,contact_line_thickness, SUB_INFO,kkp )
%UNTITLED2 이 함수의 요약 설명 위치
%   자세한 설명 위치

% fix twist
W = SUB_INFO(1);
G = SUB_INFO(2);
% G = 0;
H =SUB_INFO(3);
CB_coord = convert2_multiarray(X_cb);

%%
% CB_coord = Fix_twist(CB_coord);
CB_coord_next = CB_coord;
CB_coord_next(1:end-1,:) = CB_coord(2:end,:);
CB_coord_next(end,:) = CB_coord(1,:);

vec_node = CB_coord_next - CB_coord;

Len_vec = sqrt(vec_node(:,1).^2 + vec_node(:,2).^2);
Len_avg = mean(Len_vec);


CB_coord_up = CB_coord(find(CB_coord(:,2)>contact_line_thickness),:);
CB_coord_up_next = CB_coord_up;
CB_coord_up_next(1:end-1,:) = CB_coord_up(2:end,:);
CB_coord_up_next(end,:) = CB_coord_up(1,:);

vec_node_up = CB_coord_up_next - CB_coord_up;

Len_vec_up = sqrt(vec_node_up(:,1).^2 + vec_node_up(:,2).^2);
Len_vec_up_sum_max_deleted = sum(Len_vec_up) - max(Len_vec_up);

Len_avg1 = Len_vec_up_sum_max_deleted/(size(vec_node_up,1)-1);


% CB_end_node = size(CB_coord,1);



%%

% if kkp < 2
% CB_coord(:,2) = max(CB_coord(:,2),0.5);
% end


N_node = size(CB_coord,1);

%     CB_coord(:,2) = max(CB_coord(:,2),0.5);
CB_coord_test = CB_coord;
Node_count = 1;
while 1
%     CB_coord = Fix_twist(CB_coord);
    Node_end = size(CB_coord,1);
%     [Node_count Node_end]
   if Node_count == Node_end+1;
        break;
   end
    
    
    Node_coord = CB_coord(Node_count,:);
    
    

        
    if Node_count == Node_end;
        Node_coord_next = CB_coord(1,:);
    else
        Node_coord_next = CB_coord(Node_count+1,:);
    end
    
    Vec_node = Node_coord_next - Node_coord;
    Len_vec_node = norm(Vec_node);
    
    F_coord = calc_F_segment(Node_coord(1),Node_coord(2),alpha, contact_line_thickness,SUB_INFO);
    F_coord_next = calc_F_segment(Node_coord_next(1),Node_coord_next(2),alpha, contact_line_thickness,SUB_INFO);
    
    if Node_coord(2) < contact_line_thickness || Node_coord_next(2) < contact_line_thickness
         
        if Len_vec_node > 0.2*(W)
            CB_coord_test_F = CB_coord(1:Node_count,:);
            CB_coord_test_R = CB_coord(Node_count+1:Node_end,:);
            CB_coord = [CB_coord_test_F; 1/2* (Node_coord_next + Node_coord) ; CB_coord_test_R];
        elseif  Len_vec_node < 0.1*(W)
            if Node_count == Node_end;
                CB_coord(1,:) = [];
            else
                CB_coord(Node_count+1,:) = [];
            end
        else
            Node_count = Node_count + 1;
        end

    else
            Node_count = Node_count + 1;

    end
    
end

Node_count2=1;
while 1

    Node_end = size(CB_coord,1);
   if Node_count2 == Node_end+1;
        break;
   end
    
    
    Node_coord = CB_coord(Node_count2,:);
    
    

        
    if Node_count2 == Node_end;
        Node_coord_next = CB_coord(1,:);
    else
        Node_coord_next = CB_coord(Node_count2+1,:);
    end
    
    Vec_node = Node_coord_next - Node_coord;
    Len_vec_node = norm(Vec_node);
    if Node_coord(2) > contact_line_thickness && Node_coord_next(2) > contact_line_thickness
        if Len_vec_node > sqrt(2)*Len_avg1
            CB_coord_test_F = CB_coord(1:Node_count2,:);
            CB_coord_test_R = CB_coord(Node_count2+1:Node_end,:);
            CB_coord = [CB_coord_test_F; 1/2* (Node_coord_next + Node_coord) ; CB_coord_test_R];
        elseif  Len_vec_node < Len_avg1/sqrt(2)
            if Node_count2 == Node_end;
                CB_coord(1,:) = [];
            else
                CB_coord(Node_count2+1,:) = [];
            end
        else
            Node_count2 = Node_count2 + 1;
        end
    else
           Node_count2 = Node_count2 + 1;

    end

end

    
    CB_coord_out  = CB_coord;
    LU = [Len_avg1*sqrt(2) Len_avg1/sqrt(2)];
    X_cb_out = convert2_1Darray(CB_coord_out);

end



