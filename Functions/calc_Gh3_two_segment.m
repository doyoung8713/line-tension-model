function [ Gh3_seg ] = calc_Gh3_one_segment(x1,x2,y1,y2,alpha,contact_line_thickness,SUB_INFO,LU)
%UNTITLED3 이 함수의 요약 설명 위치
%   자세한 설명 위치
W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);

Lmin = 0.1*W;
Lmax = 0.2*W;
     
% Lmin_1 = LU(2);
% Lmax_1 = LU(1);




if sqrt((x2-x1)^2 + (y2-y1)^2) < Lmin
    [F1,dF1dx1,dF1dy1] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
    [F2,dF2dx2,dF2dy2] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);
    L = sqrt((x2-x1)^2+ (y2-y1)^2);
    dLdx1 = -(x2-x1)/L;
    dLdx2 = (x2-x1)/L;
    dLdy1 = -(y2-y1)/L;
    dLdy2 = (y2-y1)/L;
    k = Lmin;
%     
    Gh3_seg_x1 = dF1dx1*F2*(L-k)^2 + F1*F2*2*(L-k)*(dLdx1); 
    Gh3_seg_x2 = F1*dF2dx2*(L-k)^2 + F1*F2*2*(L-k)*(dLdx2); 
    Gh3_seg_y1 = dF1dy1*F2*(L-k)^2 + F1*F2*2*(L-k)*(dLdy1); 
    Gh3_seg_y2 = F1*dF2dy2*(L-k)^2 + F1*F2*2*(L-k)*(dLdy2); 
    Gh3_seg_0 = [Gh3_seg_x1 Gh3_seg_x2 Gh3_seg_y1 Gh3_seg_y2];
    
elseif sqrt((x2-x1)^2 + (y2-y1)^2) > Lmax
    [F1,dF1dx1,dF1dy1] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
    [F2,dF2dx2,dF2dy2] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);
    L = sqrt((x2-x1)^2+ (y2-y1)^2);
    dLdx1 = -(x2-x1)/L;
    dLdx2 = (x2-x1)/L;
    dLdy1 = -(y2-y1)/L;
    dLdy2 = (y2-y1)/L;
    k = Lmax;
    
    Gh3_seg_x1 = dF1dx1*F2*(L-k)^2 + F1*F2*2*(L-k)*(dLdx1); 
    Gh3_seg_x2 = F1*dF2dx2*(L-k)^2 + F1*F2*2*(L-k)*(dLdx2); 
    Gh3_seg_y1 = dF1dy1*F2*(L-k)^2 + F1*F2*2*(L-k)*(dLdy1); 
    Gh3_seg_y2 = F1*dF2dy2*(L-k)^2 + F1*F2*2*(L-k)*(dLdy2); 
    Gh3_seg_0 = [Gh3_seg_x1 Gh3_seg_x2 Gh3_seg_y1 Gh3_seg_y2];
else
    Gh3_seg_0 = [0 0 0 0];
end



% if sqrt((x2-x1)^2 + (y2-y1)^2) < Lmin
%     [F1,dF1dx1,dF1dy1] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%     [F2,dF2dx2,dF2dy2] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);
%     L = sqrt((x2-x1)^2+ (y2-y1)^2);
%     dLdx1 = -(x2-x1)/L;
%     dLdx2 = (x2-x1)/L;
%     dLdy1 = -(y2-y1)/L;
%     dLdy2 = (y2-y1)/L;
%     k = Lmin;
%     
%     Gh3_seg_x1 = dF1dx1*F2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdx1); 
%     Gh3_seg_x2 = F1*dF2dx2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdx2); 
%     Gh3_seg_y1 = dF1dy1*F2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdy1); 
%     Gh3_seg_y2 = F1*dF2dy2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdy2); 
%     Gh3_seg_0 = [Gh3_seg_x1 Gh3_seg_x2 Gh3_seg_y1 Gh3_seg_y2];
%     
% elseif sqrt((x2-x1)^2 + (y2-y1)^2) > Lmax
%     [F1,dF1dx1,dF1dy1] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%     [F2,dF2dx2,dF2dy2] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);
%     L = sqrt((x2-x1)^2+ (y2-y1)^2);
%     dLdx1 = -(x2-x1)/L;
%     dLdx2 = (x2-x1)/L;
%     dLdy1 = -(y2-y1)/L;
%     dLdy2 = (y2-y1)/L;
%     k = Lmax;
%     
%     Gh3_seg_x1 = dF1dx1*F2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdx1); 
%     Gh3_seg_x2 = F1*dF2dx2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdx2); 
%     Gh3_seg_y1 = dF1dy1*F2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdy1); 
%     Gh3_seg_y2 = F1*dF2dy2*(L-k)^2 + (F1*F2)*2*(L-k)*(dLdy2); 
%     Gh3_seg_0 = [Gh3_seg_x1 Gh3_seg_x2 Gh3_seg_y1 Gh3_seg_y2];
% else
%     Gh3_seg_0 = [0 0 0 0];
% end
   

% if sqrt((x2-x1)^2 + (y2-y1)^2) < Lmin_1
%     [F1,dF1dx1,dF1dy1] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%     [F2,dF2dx2,dF2dy2] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);
%     L = sqrt((x2-x1)^2+ (y2-y1)^2);
%     dLdx1 = -(x2-x1)/L;
%     dLdx2 = (x2-x1)/L;
%     dLdy1 = -(y2-y1)/L;
%     dLdy2 = (y2-y1)/L;
%     k = Lmin_1;
%     
%     Gh3_seg_x1 = -1/2*dF1dx1*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdx1); 
%     Gh3_seg_x2 = -1/2*dF2dx2*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdx2); 
%     Gh3_seg_y1 = -1/2*dF1dy1*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdy1); 
%     Gh3_seg_y2 = -1/2*dF2dy2*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdy2); 
%     Gh3_seg_1 = [Gh3_seg_x1 Gh3_seg_x2 Gh3_seg_y1 Gh3_seg_y2];
%     
% elseif sqrt((x2-x1)^2 + (y2-y1)^2) > Lmax_1
%     [F1,dF1dx1,dF1dy1] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%     [F2,dF2dx2,dF2dy2] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);
%     L = sqrt((x2-x1)^2+ (y2-y1)^2);
%     dLdx1 = -(x2-x1)/L;
%     dLdx2 = (x2-x1)/L;
%     dLdy1 = -(y2-y1)/L;
%     dLdy2 = (y2-y1)/L;
%     k = Lmax_1;
%     
%     Gh3_seg_x1 = -1/2*dF1dx1*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdx1); 
%     Gh3_seg_x2 = -1/2*dF2dx2*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdx2); 
%     Gh3_seg_y1 = -1/2*dF1dy1*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdy1); 
%     Gh3_seg_y2 = -1/2*dF2dy2*(L-k)^2 + 1/2*(2-F1-F2)*2*(L-k)*(dLdy2); 
%     Gh3_seg_1 = [Gh3_seg_x1 Gh3_seg_x2 Gh3_seg_y1 Gh3_seg_y2];
% else
%     Gh3_seg_1 = [0 0 0 0];
% end
    Gh3_seg = Gh3_seg_0 ;
%     Gh3_seg = Gh3_seg_0 + Gh3_seg_1;
end

