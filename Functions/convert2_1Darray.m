%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Scripted by "Keonwook Kang" in Yonsei University
%%% Object : Convert to 1D array X
%%% Input :  Coordinates
%%% Output : 1D array matrix
%%% Date : 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = convert2_1Darray(X)
% Convert (np) x (ndim) matrix X to 1D array 

np = size(X,1); ndim = size(X,2);
ndof = np*ndim; 

temp = zeros(ndof,1); 
for i=1:ndim
    temp(i:ndim:ndof) = X(:,i);
end
ret = temp;

return