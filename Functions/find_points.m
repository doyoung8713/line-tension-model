function [ regions,points,gshape_sorted ] = find_points( x1,x2,y1,y2 ,SUB_INFO)
%UNTITLED4 이 함수의 요약 설명 위치
%   자세한 설명 위치
W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);

points = [x1 y1 0];
gshape = [1 0 0 0 0 0 1 0];

%%% test if the line has contact with boundaries





%%%%% 1. y=-x, 0<x<W/2
if (y2+x2)*(y1+x1) < 0 %%% y=x
    P=0;
    
    xc = (x1*y2-x2*y1+(x2-x1)*P)/(x2-x1+y2-y1);
    yc = -xc+P ;
    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);


    
    if 0<=xc && xc<=W/2 &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = (y2-P) /(x2-x1+y2-y1) + (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;
        dxp_dx2 = (-y1+P)/(x2-x1+y2-y1) - (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;
        dxp_dy1 = (-x2)  /(x2-x1+y2-y1) + (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;
        dxp_dy2 = (x1)   /(x2-x1+y2-y1) - (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;

        dyp_dx1 =-dxp_dx1;
        dyp_dx2 =-dxp_dx2;
        dyp_dy1 =-dxp_dy1;
        dyp_dy2 =-dxp_dy2;
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end
    
end
%%%%% 2. y=x-W, W/2<x<W
if (y2-x2+W)*(y1-x1+W) < 0 %%% y=x
    P=-W;
        
    xc = (-x1*y2+x2*y1-(x2-x1)*P)/(x2-x1-y2+y1);
    yc = xc+P ;
    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);


    
    if W/2<xc && xc<=W &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = (-y2+P)/(x2-x1-y2+y1) + (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;
        dxp_dx2 = (y1-P) /(x2-x1-y2+y1) - (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;
        dxp_dy1 = (x2)   /(x2-x1-y2+y1) - (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;
        dxp_dy2 = (-x1)  /(x2-x1-y2+y1) - (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;

        dyp_dx1 =dxp_dx1;
        dyp_dx2 =dxp_dx2;
        dyp_dy1 =dxp_dy1;
        dyp_dy2 =dxp_dy2;
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end
    
end
%%%%% 3. x=W/2, y<-W/2
if (x2-W/2)*(x1-W/2) < 0 %%% y=x
    P=W/2;
    yc = (y2-y1)*(P-x1)/(x2-x1) + y1;
    xc = P;
    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);
  
    if yc<-W/2  &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = 0;
        dxp_dx2 = 0;
        dxp_dy1 = 0;
        dxp_dy2 = 0;

        dyp_dx1 =-(y2-y1)/(x2-x1) + (y2-y1)*(P-x1)/(x2-x1)^2;
        dyp_dx2 =-(y2-y1)*(P-x1)/(x2-x1)^2;
        dyp_dy1 =(x2-P)/(x2-x1);
        dyp_dy2 =(-x1+P)/(x2-x1);
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end

end
%%%%% 4. x=W, 0<y 
if (x2-W)*(x1-W) < 0 %%% y=x
    P=W;
    yc = (y2-y1)*(P-x1)/(x2-x1) + y1;
    xc = P;
    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);
  
    if 0<yc &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = 0;
        dxp_dx2 = 0;
        dxp_dy1 = 0;
        dxp_dy2 = 0;

        dyp_dx1 =-(y2-y1)/(x2-x1) + (y2-y1)*(P-x1)/(x2-x1)^2;
        dyp_dx2 =-(y2-y1)*(P-x1)/(x2-x1)^2;
        dyp_dy1 =(x2-P)/(x2-x1);
        dyp_dy2 =(-x1+P)/(x2-x1);
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end
end
%%%%% 5. y=0, W<X<W+G
if (y2)*(y1) < 0 %%% y=x
    P=0;
    xc = (x2-x1)/(y2-y1)*(P-y1) + x1;
    yc = P;
    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);
    
        if W<xc && xc<=W+G &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = (y2-P)/(y2-y1);
        dxp_dx2 = (-y1+P)/(y2-y1);
        dxp_dy1 = -(x2-x1)/(y2-y1) + (x2-x1)*(P-y1)/(y2-y1)^2;
        dxp_dy2 = -(x2-x1)*(P-y1)/(y2-y1)^2;

        dyp_dx1 = 0;
        dyp_dx2 = 0;
        dyp_dy1 = 0;
        dyp_dy2 = 0;
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end


end
%%%%% 6. y=x-W-H, W/2<x<W+G/2
if (y2-x2+W+H)*(y1-x1+W+H) < 0 %%% y=x
    P=-W-H;
    xc = (-x1*y2+x2*y1-(x2-x1)*P)/(x2-x1-y2+y1);
    yc = xc+P ;
    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);


    
    if W/2<=xc && xc<=W+G/2 &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = (-y2+P)/(x2-x1-y2+y1) + (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;
        dxp_dx2 = (y1-P) /(x2-x1-y2+y1) - (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;
        dxp_dy1 = (x2)   /(x2-x1-y2+y1) - (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;
        dxp_dy2 = (-x1)  /(x2-x1-y2+y1) - (-x1*y2+x2*y1 - (x2-x1)*P)/(x2-x1-y2+y1)^2;

        dyp_dx1 =dxp_dx1;
        dyp_dx2 =dxp_dx2;
        dyp_dy1 =dxp_dy1;
        dyp_dy2 =dxp_dy2;
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end
end
%%%%% 7. y=-x+W+G-H, W+G/2<x<W+G
if (y2+x2-W-G+H)*(y1+x1-W-G+H) < 0 %%% y=x
    P=W+G-H;
        
    xc = (y2-P)/(x2-x1+y2-y1) + (x1*y2 - x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2; 
    yc = -xc+P ;
    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);


    
    if W+G/2<xc && xc<=W+G &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = (y2-P)/(x2-x1+y2-y1)  + (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;
        dxp_dx2 = (-y1+P)/(x2-x1+y2-y1) - (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;
        dxp_dy1 = (-x2)/(x2-x1+y2-y1)   + (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;
        dxp_dy2 = (x1)/(x2-x1+y2-y1)    - (x1*y2-x2*y1 + (x2-x1)*P)/(x2-x1+y2-y1)^2;

        dyp_dx1 =-dxp_dx1;
        dyp_dx2 =-dxp_dx2;
        dyp_dy1 =-dxp_dy1;
        dyp_dy2 =-dxp_dy2;
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end
    
end
%%%%% 8. x=W+G/2 G/2-H<y
if (x2-W-G/2)*(x1-W-G/2) < 0 %%% 
    P=W+G/2;
    xc = P;
    yc = (y2-y1)*(P-x1)/(x2-x1) + y1;

    t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);


    
    if G/2-H < yc &&  (0<t && t<1)  
        points(end+1,:) = [xc,yc,t];

        dxp_dx1 = 0;
        dxp_dx2 = 0;
        dxp_dy1 = 0;
        dxp_dy2 = 0;

        dyp_dx1 =-(y2-y1)/(x2-x1) + (y2-y1)*(P-x1)/(x2-x1)^2;
        dyp_dx2 =-(y2-y1)*(P-x1)/(x2-x1)^2;
        dyp_dy1 =(x2-P)/(x2-x1);
        dyp_dy2 =(-x1+P)/(x2-x1);
        gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
    end
end



    

points(end+1,:) = [x2,y2,1];
gshape(end+1,:)  = [0 1 0 0 0 0 0 1];
[points ind] = sortrows(points,3);
gshape_sorted = gshape(ind,:);


%%%%%%% Regions %%%%%%%%%

N_point = size(points,1);

for k =1 : N_point-1;
    mid_point = 1/2 * (points(k,1:2) + points(k+1,1:2));
    regions(k) = find_region(mid_point(1),mid_point(2),SUB_INFO);
end
    
    
    







end

