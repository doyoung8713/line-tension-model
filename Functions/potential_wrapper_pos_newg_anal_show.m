function [Etot,Gtot,Gh2_o,Gs_o] = potential_wrapper_pos_newg_anal_show(X_cb,Vol0,lambda1,rk1,lambda2,rk2,lambda3,rk3,contact_line_thickness,gamma_1,gamma_2,alpha,grav,rho,SUB_INFO,LU)

Etot = calc_tot_E_anal(X_cb,Vol0,lambda1,rk1,lambda2,rk2,lambda3,rk3,contact_line_thickness,gamma_1,gamma_2,alpha,grav,rho,SUB_INFO,LU);

CB_coord = convert2_multiarray(X_cb);
N_node = size(CB_coord,1);
xp = CB_coord(:,1);

for i = 1:N_node

    
%     cutoff_up = 1.8*contact_line_thickness ;
%     cutoff_down = 0.2*contact_line_thickness ;
    
    if i==1;
        xA = CB_coord(N_node,1);
        yA = CB_coord(N_node,2);
    else
        xA = CB_coord(i-1,1);
        yA = CB_coord(i-1,2);
    end
    
    xB = CB_coord(i,1);
    yB = CB_coord(i,2);
    
    if i==N_node;
        xC = CB_coord(1,1);
        yC = CB_coord(1,2);
    else
        xC = CB_coord(i+1,1);
        yC = CB_coord(i+1,2);
    end
  

%%% surf
%     for j = 1:2
%     if j ==1
%     GsurfA(i,j) =  ( calc_period_energy( xA,xB+pert,yA,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) -  calc_period_energy( xA,xB-pert,yA,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) / 2/pert;
%     GsurfC(i,j) =  ( calc_period_energy( xC,xB+pert,yC,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) -  calc_period_energy( xC,xB-pert,yC,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) / 2/pert;
% %     GsurfA(i,j) =  (calc_one_line_energy( xA,xB+pert,yA,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) - calc_one_line_energy( xA,xB,yA,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) /pert;
% %     GsurfC(i,j) =  (calc_one_line_energy( xC,xB+pert,yC,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) - calc_one_line_energy( xC,xB,yC,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) /pert;
%     Gsurf(i,j) = GsurfA(i,j) + GsurfC(i,j);
%     
% 
%     
%     else
%         
%     GsurfA(i,j) =  ( calc_period_energy( xA,xB,yA,yB+pert,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) -  calc_period_energy( xA,xB,yA,yB-pert,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) / 2/pert;
%     GsurfC(i,j) =  ( calc_period_energy( xC,xB,yC,yB+pert,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) -  calc_period_energy( xC,xB,yC,yB-pert,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) / 2/pert;
% %     GsurfA(i,j) =  (calc_one_line_energy( xA,xB,yA,yB+pert,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) - calc_one_line_energy( xA,xB,yA,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) /pert;
% %     GsurfC(i,j) =  (calc_one_line_energy( xC,xB,yC,yB+pert,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) - calc_one_line_energy( xC,xB,yC,yB,gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO) ) /pert;
%     
%     Gsurf(i,j) = GsurfA(i,j) + GsurfC(i,j);
%         
%     end
%     end

% % %%% surf
    [ G_tot_sum_analA ] = calc_grad_anal_period( xA,xB,yA,yB,gamma_1,gamma_2,alpha,contact_line_thickness,SUB_INFO);
    [ G_tot_sum_analC ] = calc_grad_anal_period( xC,xB,yC,yB,gamma_1,gamma_2,alpha,contact_line_thickness,SUB_INFO);
    GsurfA(i,:) = [G_tot_sum_analA(2) G_tot_sum_analA(4)];
    GsurfC(i,:) = [G_tot_sum_analC(2) G_tot_sum_analC(4)];
    Gsurf(i,:) = GsurfA(i,:) + GsurfC(i,:);

    

    Gs_o = convert2_1Darray(Gsurf);
end
%%% Lagrange
    [Vol,G_Vol] = calc_volume(CB_coord);
    h1 =  (Vol - Vol0) ;
       
    h2 = calc_h2_one(CB_coord,SUB_INFO);
    
    G_h1 =  G_Vol;
    
    G_h2 = calc_Gh2_one(CB_coord,SUB_INFO);
    


    h3 = calc_h3_two(CB_coord,alpha,contact_line_thickness,SUB_INFO,LU);
    G_h3  = calc_Gh3_two(CB_coord,alpha,contact_line_thickness,SUB_INFO,LU);

    G_LAGRANGE = -lambda1 * G_h1 + rk1*h1 * G_h1 - lambda2 * G_h2 + rk2*h2 * G_h2 -lambda3 * G_h3 + rk3*h3 * G_h3;
    Gh2_o = convert2_1Darray(G_LAGRANGE);   
    
    
%%% grav
   [ C,GCx,GCy ] = calc_centroid( CB_coord );
%     G_gravity = grav*(   G_Vol.*( C(2)+C(2).^2) + Vol*(GCy+2.*C(2) .* GCy) );
    G_gravity = grav*(Vol0*GCy + G_Vol*C(2));
    
    
    Gtot = Gsurf + G_LAGRANGE + G_gravity;
    

Gtot = convert2_1Darray(Gtot);

end
