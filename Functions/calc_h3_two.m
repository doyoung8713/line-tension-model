function [ h3 ] = calc_h3_one(CB_coord,alpha, contact_line_thickness,SUB_INFO,LU)
%UNTITLED3 이 함수의 요약 설명 위치
%   자세한 설명 위치
W = SUB_INFO(1);
G = SUB_INFO(2);
H =SUB_INFO(3);

N_node = size(CB_coord,1);
h3_data = 0;



for sq = 1:N_node;
    
    if sq == N_node;
        x2 = CB_coord(1,1);
        y2 = CB_coord(1,2);
        x1 = CB_coord(sq,1);
        y1 = CB_coord(sq,2);
    else
    x2 = CB_coord(sq+1,1);   
    y2 = CB_coord(sq+1,2);
    x1 = CB_coord(sq,1);
    y1 = CB_coord(sq,2);
    end
    
    
     Lmin = 0.1*W;
     Lmax = 0.2*W;
%      
%      Lmin_1 = LU(2);
%      Lmax_1 = LU(1);
%      

    if sqrt((x2-x1)^2 + (y2-y1)^2) < Lmin
        [F1,dF1dx,dF1dy] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
        [F2,dF2dx,dF2dy] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);            
        h3_data_0(sq) =  F1*F2*(sqrt((x2-x1)^2 + (y2-y1)^2)-Lmin)^2;
    elseif sqrt((x2-x1)^2 + (y2-y1)^2) > Lmax
        [F1,dF1dx,dF1dy] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
        [F2,dF2dx,dF2dy] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);    
        h3_data_0(sq) =  F1*F2*(sqrt((x2-x1)^2 + (y2-y1)^2)-Lmax)^2;
    else
        h3_data_0(sq) = 0;
    end    



%     [F1,dF1dx,dF1dy] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%     [F2,dF2dx,dF2dy] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);    
    
%     if sqrt((x2-x1)^2 + (y2-y1)^2) < Lmin
%         [F1,dF1dx,dF1dy] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%         [F2,dF2dx,dF2dy] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);            
%         h3_data_0(sq) = (F1*F2) * (sqrt((x2-x1)^2 + (y2-y1)^2)-Lmin)^2;
%     elseif sqrt((x2-x1)^2 + (y2-y1)^2) > Lmax
%         [F1,dF1dx,dF1dy] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%         [F2,dF2dx,dF2dy] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);    
%         h3_data_0(sq) = (F1*F2) * (sqrt((x2-x1)^2 + (y2-y1)^2)-Lmax)^2;
%     else
%         h3_data_0(sq) = 0;
%     end
    
%     
%     if sqrt((x2-x1)^2 + (y2-y1)^2) <Lmin_1
%         [F1,dF1dx,dF1dy] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%         [F2,dF2dx,dF2dy] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);    
%         h3_data_1(sq) = 1/2*(2-F1-F2) * (sqrt((x2-x1)^2 + (y2-y1)^2)-Lmin_1)^2;
%     elseif sqrt((x2-x1)^2 + (y2-y1)^2) > Lmax_1
%         [F1,dF1dx,dF1dy] = calc_F_segment( x1,y1,alpha,contact_line_thickness,SUB_INFO);
%         [F2,dF2dx,dF2dy] = calc_F_segment( x2,y2,alpha,contact_line_thickness,SUB_INFO);    
%         h3_data_1(sq) = 1/2*(2-F1-F2) * (sqrt((x2-x1)^2 + (y2-y1)^2)-Lmax_1)^2;
%     else
%         h3_data_1(sq) = 0;
%     end

    
    
    
end
% h3_data = h3_data_0 + h3_data_1;
h3_data = h3_data_0;
h3= sum(h3_data);
% h2 = 0;
        

    


end

