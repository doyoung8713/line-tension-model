function [ G_anal ] = calc_grad_anal( x1,x2,y1,y2,gamma_1,gamma_2,alpha,contact_line_thickness,XorY)
%UNTITLED2 이 함수의 요약 설명 위치
%   자세한 설명 위치
    if XorY ==1   %%% 1 for gamma(x)
        if alpha*(x1-contact_line_thickness)>10 && alpha*(x2-contact_line_thickness)>10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
       elseif alpha*(x1-contact_line_thickness)<-10 && alpha*(x2-contact_line_thickness)<-10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
        
        elseif ((x1-x2)/(y1-y2))^2<eps
        
            G_CB_X1_anal = sqrt((y2-y1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1-contact_line_thickness)))^2) * alpha;
            G_CB_X2_anal = sqrt((y2-y1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1-contact_line_thickness)))^2) * alpha;

            G_CB_Y1_anal= - (y2-y1) / sqrt((y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1-contact_line_thickness)));
            G_CB_Y2_anal=   (y2-y1) / sqrt((y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1-contact_line_thickness)));

        else
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(x2-x1) *log(cosh(alpha*(x2-contact_line_thickness))/cosh(alpha*(x1-contact_line_thickness)));
        dFdx1 = (gamma_1-gamma_2)/2/alpha * ((x2-x1)^(-2) * log(cosh(alpha*(x2-contact_line_thickness))/(cosh(alpha*(x1-contact_line_thickness)))) + ...
(x2-x1)^(-1) * (-sinh(alpha*(x1-contact_line_thickness))/cosh(alpha*(x1-contact_line_thickness))*alpha));
        dFdx2 = (gamma_1-gamma_2)/2/alpha * (-(x2-x1)^(-2) * log(cosh(alpha*(x2-contact_line_thickness))/(cosh(alpha*(x1-contact_line_thickness)))) + ...
(x2-x1)^(-1) * (sinh(alpha*(x2-contact_line_thickness))/cosh(alpha*(x2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx1  ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx2;


        G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  ;
        G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F ;
        end
    
        

   elseif XorY ==2    %%% 2 for gamma(y)
       
        if alpha*(y1-contact_line_thickness)>10 && alpha*(y2-contact_line_thickness)>10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1; 
            
       elseif alpha*(y1-contact_line_thickness)<-10 && alpha*(y2-contact_line_thickness)<-10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;            
        elseif ((y1-y2)/(x1-x2))^2<eps

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(y1-contact_line_thickness)));
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(y1-contact_line_thickness)));

        G_CB_Y1_anal= sqrt((x2-x1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(y1-contact_line_thickness)))^2) * alpha;
        G_CB_Y2_anal= sqrt((x2-x1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(y1-contact_line_thickness)))^2) * alpha;




        else
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(y2-y1) *log(cosh(alpha*(y2-contact_line_thickness))/cosh(alpha*(y1-contact_line_thickness)));
        dFdy1 = (gamma_1-gamma_2)/2/alpha * ((y2-y1)^(-2) * log(cosh(alpha*(y2-contact_line_thickness))/(cosh(alpha*(y1-contact_line_thickness)))) + ...
(y2-y1)^(-1) * (-sinh(alpha*(y1-contact_line_thickness))/cosh(alpha*(y1-contact_line_thickness))*alpha));
        dFdy2 = (gamma_1-gamma_2)/2/alpha * (-(y2-y1)^(-2) * log(cosh(alpha*(y2-contact_line_thickness))/(cosh(alpha*(y1-contact_line_thickness)))) + ...
(y2-y1)^(-1) * (sinh(alpha*(y2-contact_line_thickness))/cosh(alpha*(y2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F ;

        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy1;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy2;
        end
   
 
    elseif XorY ==3 %%% 3 for gamma(x+y) 
%         (y2-y1-x1+x2)^2
        if alpha*(x1+y1-contact_line_thickness)>10 && alpha*(x2+y2-contact_line_thickness)>10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1   ;  
       elseif alpha*(x1+y1-contact_line_thickness)<-10 && alpha*(x2+y2-contact_line_thickness)<-10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
        elseif ((y1-y2)/(x1-x2)+1 )^2<eps

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha;

        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha;



        else
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(x2+y2-x1-y1) *log(cosh(alpha*(x2+y2-contact_line_thickness))/cosh(alpha*(x1+y1-contact_line_thickness)));

        dFdx1 = (gamma_1-gamma_2)/2/alpha * ((x2+y2-x1-y1)^(-2) * log(cosh(alpha*(x2+y2-contact_line_thickness))/(cosh(alpha*(x1+y1-contact_line_thickness)))) + ...
(x2+y2-x1-y1)^(-1) * (-sinh(alpha*(x1+y1-contact_line_thickness))/cosh(alpha*(x1+y1-contact_line_thickness))*alpha));

        dFdx2 = (gamma_1-gamma_2)/2/alpha * (-(x2+y2-x1-y1)^(-2) * log(cosh(alpha*(x2+y2-contact_line_thickness))/(cosh(alpha*(x1+y1-contact_line_thickness)))) + ...
(x2+y2-x1-y1)^(-1) * ( sinh(alpha*(x2+y2-contact_line_thickness))/cosh(alpha*(x2+y2-contact_line_thickness))*alpha));

        dFdy1 = (gamma_1-gamma_2)/2/alpha * ((x2+y2-x1-y1)^(-2) * log(cosh(alpha*(x2+y2-contact_line_thickness))/(cosh(alpha*(x1+y1-contact_line_thickness)))) + ...
(x2+y2-x1-y1)^(-1) * (-sinh(alpha*(x1+y1-contact_line_thickness))/cosh(alpha*(x1+y1-contact_line_thickness))*alpha));

        dFdy2 = (gamma_1-gamma_2)/2/alpha * (-(x2+y2-x1-y1)^(-2) * log(cosh(alpha*(x2+y2-contact_line_thickness))/(cosh(alpha*(x1+y1-contact_line_thickness)))) + ...
(x2+y2-x1-y1)^(-1) * ( sinh(alpha*(x2+y2-contact_line_thickness))/cosh(alpha*(x2+y2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx1; 
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx2 ;

          
        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy1;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy2;

        end
    else %%% else for gamma(-x+y) 
        if alpha*(-x1+y1-contact_line_thickness)>10 && alpha*(-x2+y2-contact_line_thickness)>10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_1;              
%         elseif (y2-y1-x2+x1)^2<eps;
       elseif alpha*(-x1+y1-contact_line_thickness)<-10 && alpha*(-x2+y2-contact_line_thickness)<-10
            G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;

            G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
            G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 + (y2-y1)^2) * gamma_2;
        elseif ((y1-y2)/(x1-x2)-1 )^2<eps

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * (-alpha) ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * (-alpha);

        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * alpha;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * alpha;



        else
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(-x2+y2+x1-y1) *log(cosh(alpha*(-x2+y2-contact_line_thickness))/cosh(alpha*(-x1+y1-contact_line_thickness)));

        dFdx1 = (gamma_1-gamma_2)/2/alpha * (-(-x2+y2+x1-y1)^(-2) * log(cosh(alpha*(-x2+y2-contact_line_thickness))/(cosh(alpha*(-x1+y1-contact_line_thickness)))) + ...
(-x2+y2+x1-y1)^(-1) * (sinh(alpha*(-x1+y1-contact_line_thickness))/cosh(alpha*(-x1+y1-contact_line_thickness))*alpha));

        dFdx2 = (gamma_1-gamma_2)/2/alpha * ((-x2+y2+x1-y1)^(-2) * log(cosh(alpha*(-x2+y2-contact_line_thickness))/(cosh(alpha*(-x1+y1-contact_line_thickness)))) + ...
(-x2+y2+x1-y1)^(-1) * (-sinh(alpha*(-x2+y2-contact_line_thickness))/cosh(alpha*(-x2+y2-contact_line_thickness))*alpha));

        dFdy1 = (gamma_1-gamma_2)/2/alpha * ((-x2+y2+x1-y1)^(-2) * log(cosh(alpha*(-x2+y2-contact_line_thickness))/(cosh(alpha*(-x1+y1-contact_line_thickness)))) + ...
(-x2+y2+x1-y1)^(-1) * (-sinh(alpha*(-x1+y1-contact_line_thickness))/cosh(alpha*(-x1+y1-contact_line_thickness))*alpha));

        dFdy2 = (gamma_1-gamma_2)/2/alpha * (-(-x2+y2+x1-y1)^(-2) * log(cosh(alpha*(-x2+y2-contact_line_thickness))/(cosh(alpha*(-x1+y1-contact_line_thickness)))) + ...
(-x2+y2+x1-y1)^(-1) * ( sinh(alpha*(-x2+y2-contact_line_thickness))/cosh(alpha*(-x2+y2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx1 ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx2 ;

          
        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy1;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy2;



        


        end
    end
    G_anal = [G_CB_X1_anal G_CB_X2_anal G_CB_Y1_anal G_CB_Y2_anal];



end

