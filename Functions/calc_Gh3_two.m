function [ Gh3 ] = calc_Gh3_one(CB_coord,alpha,contact_line_thickness,SUB_INFO,LU)
%UNTITLED3 이 함수의 요약 설명 위치
%   자세한 설명 위치
W = SUB_INFO(1);
G = SUB_INFO(2);
H =SUB_INFO(3);

N_node = size(CB_coord,1);
% h3_data = 0;



for sq = 1:N_node;
    
    if sq==1;
        xA = CB_coord(N_node,1);
        yA = CB_coord(N_node,2);
    else
        xA = CB_coord(sq-1,1);
        yA = CB_coord(sq-1,2);
    end
    
    xB = CB_coord(sq,1);
    yB = CB_coord(sq,2);
    
    if sq==N_node;
        xC = CB_coord(1,1);
        yC = CB_coord(1,2);
    else
        xC = CB_coord(sq+1,1);
        yC = CB_coord(sq+1,2);
    end


    
    [ Gh3_segA ] = calc_Gh3_two_segment( xA,xB,yA,yB,alpha,contact_line_thickness,SUB_INFO,LU);
    [ Gh3_segC ] = calc_Gh3_two_segment( xC,xB,yC,yB,alpha,contact_line_thickness,SUB_INFO,LU);
    Gh3A(sq,:) = [Gh3_segA(2) Gh3_segA(4)];
    Gh3C(sq,:) = [Gh3_segC(2) Gh3_segC(4)];
    Gh3(sq,:) = Gh3A(sq,:) + Gh3C(sq,:);

    
end


        

    


end

