function [ h2 ] = calc_h2_one(CB_coord,SUB_INFO )
%UNTITLED3 이 함수의 요약 설명 위치
%   자세한 설명 위치
W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);


N_node = size(CB_coord,1);
h2_data = 0;
for sq = 1:N_node;
    
    IND_period = floor(CB_coord(sq,1)/(W+G));
    CB_coord(sq,1) = CB_coord(sq,1) - (W+G)*(IND_period);
    
    region = find_region(CB_coord(sq,1), CB_coord(sq,2),SUB_INFO);
    if region == 1;
        if CB_coord(sq,2) < 0;
            h2_data(sq) = CB_coord(sq,2)^2;
        end
        
    elseif region ==2
        if CB_coord(sq,1) > 0;
            h2_data(sq) = CB_coord(sq,1)^2;
        end
    elseif region ==3
        if CB_coord(sq,1) < W;
            h2_data(sq) = (W-CB_coord(sq,1))^2;
        end
    elseif region ==5
        if CB_coord(sq,2) < -H;
            h2_data(sq) = (-H-CB_coord(sq,2))^2;
        end     
%     elseif region ==6
%         h2_data(sq) = min(0,W+G-CB_coord(sq,1))^2;
%         h2_data(sq) = min(0,W+G-CB_coord(sq,1));
    end
    
%     if CB_coord(sq,2) <0;
%         h2_data(sq) = CB_coord(sq,2)^2;
%     else
%         h2_data(sq) = 0;
%     end
%     
end

h2 = sum(h2_data);
% h2 = 0;
        

    


end

