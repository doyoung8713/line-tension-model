function [ G_anal ] = calc_grad_anal( x1,x2,y1,y2,gamma_1,gamma_2,alpha,contact_line_thickness,XorY)
%UNTITLED2 이 함수의 요약 설명 위치
%   자세한 설명 위치
    if XorY ==1   %%% 1 for gamma(x)

        if ((x1-x2)/(y1-y2))^2<eps
        
            G_CB_X1_anal = sqrt((y2-y1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1-contact_line_thickness)))^2) * alpha;
            G_CB_X2_anal = sqrt((y2-y1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1-contact_line_thickness)))^2) * alpha;

            G_CB_Y1_anal= - (y2-y1) / sqrt((y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1-contact_line_thickness)));
            G_CB_Y2_anal=   (y2-y1) / sqrt((y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1-contact_line_thickness)));

        else
%             F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(x2-x1) *log(cosh(alpha*(x2-contact_line_thickness))/cosh(alpha*(x1-contact_line_thickness)));
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(x2-x1) *(lncosh(alpha*(x2-contact_line_thickness))-lncosh(alpha*(x1-contact_line_thickness)));
            
            
        dFdx1 = (gamma_1-gamma_2)/2/alpha * ((x2-x1)^(-2) * (lncosh(alpha*(x2-contact_line_thickness))-lncosh(alpha*(x1-contact_line_thickness))) + ...
(x2-x1)^(-1) * (-tanh(alpha*(x1-contact_line_thickness))*alpha));
        dFdx2 = (gamma_1-gamma_2)/2/alpha * (-(x2-x1)^(-2) *(lncosh(alpha*(x2-contact_line_thickness))-lncosh(alpha*(x1-contact_line_thickness))) + ...
(x2-x1)^(-1) * (tanh(alpha*(x2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx1  ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx2;


        G_CB_Y1_anal= - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  ;
        G_CB_Y2_anal=   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F ;
        end
    
        

   elseif XorY ==2    %%% 2 for gamma(y)
       
        
        if ((y1-y2)/(x1-x2))^2<eps

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(y1-contact_line_thickness)));
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(y1-contact_line_thickness)));

        G_CB_Y1_anal= sqrt((x2-x1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(y1-contact_line_thickness)))^2) * alpha;
        G_CB_Y2_anal= sqrt((x2-x1)^2) * (gamma_1-gamma_2)/4 * (1- (tanh(alpha*(y1-contact_line_thickness)))^2) * alpha;




        else
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(y2-y1) *(lncosh(alpha*(y2-contact_line_thickness))-lncosh(alpha*(y1-contact_line_thickness)));
        dFdy1 = (gamma_1-gamma_2)/2/alpha * ((y2-y1)^(-2) * (lncosh(alpha*(y2-contact_line_thickness))-lncosh(alpha*(y1-contact_line_thickness))) + ...
(y2-y1)^(-1) * (-tanh(alpha*(y1-contact_line_thickness))*alpha));
        dFdy2 = (gamma_1-gamma_2)/2/alpha * (-(y2-y1)^(-2) *(lncosh(alpha*(y2-contact_line_thickness))-lncosh(alpha*(y1-contact_line_thickness))) + ...
(y2-y1)^(-1) * (tanh(alpha*(y2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F ;

        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy1;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy2;
        end
   
 
    elseif XorY ==3 %%% 3 for gamma(x+y) 
%         (y2-y1-x1+x2)^2

        if ((y1-y2)/(x1-x2)+1 )^2<eps

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha;

        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(x1+y1-contact_line_thickness)))^2) * alpha;



        else
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(x2+y2-x1-y1) *(lncosh(alpha*(x2+y2-contact_line_thickness))-lncosh(alpha*(x1+y1-contact_line_thickness)));

        dFdx1 = (gamma_1-gamma_2)/2/alpha * ((x2+y2-x1-y1)^(-2) * (lncosh(alpha*(x2+y2-contact_line_thickness))-lncosh(alpha*(x1+y1-contact_line_thickness))) + ...
(x2+y2-x1-y1)^(-1) * (-tanh(alpha*(x1+y1-contact_line_thickness))*alpha));

        dFdx2 = (gamma_1-gamma_2)/2/alpha * (-(x2+y2-x1-y1)^(-2) * (lncosh(alpha*(x2+y2-contact_line_thickness))-lncosh(alpha*(x1+y1-contact_line_thickness))) + ...
(x2+y2-x1-y1)^(-1) * ( tanh(alpha*(x2+y2-contact_line_thickness))*alpha));

        dFdy1 = (gamma_1-gamma_2)/2/alpha * ((x2+y2-x1-y1)^(-2) * (lncosh(alpha*(x2+y2-contact_line_thickness))-lncosh(alpha*(x1+y1-contact_line_thickness))) + ...
(x2+y2-x1-y1)^(-1) * (-tanh(alpha*(x1+y1-contact_line_thickness))*alpha));

        dFdy2 = (gamma_1-gamma_2)/2/alpha * (-(x2+y2-x1-y1)^(-2) * (lncosh(alpha*(x2+y2-contact_line_thickness))-lncosh(alpha*(x1+y1-contact_line_thickness))) + ...
(x2+y2-x1-y1)^(-1) * ( tanh(alpha*(x2+y2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx1; 
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx2 ;

          
        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy1;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy2;

        end
    else %%% else for gamma(-x+y) 

        if ((y1-y2)/(x1-x2)-1 )^2<eps

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * (-alpha) ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * (-alpha);

        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * alpha;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * ((gamma_1 + gamma_2)/2 + (gamma_1 - gamma_2)/2 * tanh(alpha*(-x1+y1-contact_line_thickness))) ...
+ sqrt((x2-x1)^2 + (y2-y1)^2)*(gamma_1-gamma_2)/4 * (1- (tanh(alpha*(-x1+y1-contact_line_thickness)))^2) * alpha;



        else
            F = 1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)/alpha/(-x2+y2+x1-y1) *(lncosh(alpha*(-x2+y2-contact_line_thickness))-lncosh(alpha*(-x1+y1-contact_line_thickness)));

        dFdx1 = (gamma_1-gamma_2)/2/alpha * (-(-x2+y2+x1-y1)^(-2) * (lncosh(alpha*(-x2+y2-contact_line_thickness))-lncosh(alpha*(-x1+y1-contact_line_thickness))) + ...
(-x2+y2+x1-y1)^(-1) * (tanh(alpha*(-x1+y1-contact_line_thickness))*alpha));

        dFdx2 = (gamma_1-gamma_2)/2/alpha * ((-x2+y2+x1-y1)^(-2) * (lncosh(alpha*(-x2+y2-contact_line_thickness))-lncosh(alpha*(-x1+y1-contact_line_thickness))) + ...
(-x2+y2+x1-y1)^(-1) * (-tanh(alpha*(-x2+y2-contact_line_thickness))*alpha));

        dFdy1 = (gamma_1-gamma_2)/2/alpha * ((-x2+y2+x1-y1)^(-2) * (lncosh(alpha*(-x2+y2-contact_line_thickness))-lncosh(alpha*(-x1+y1-contact_line_thickness))) + ...
(-x2+y2+x1-y1)^(-1) * (-tanh(alpha*(-x1+y1-contact_line_thickness))*alpha));

        dFdy2 = (gamma_1-gamma_2)/2/alpha * (-(-x2+y2+x1-y1)^(-2) * (lncosh(alpha*(-x2+y2-contact_line_thickness))-lncosh(alpha*(-x1+y1-contact_line_thickness))) + ...
(-x2+y2+x1-y1)^(-1) * ( tanh(alpha*(-x2+y2-contact_line_thickness))*alpha));

        G_CB_X1_anal = - (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx1 ;
        G_CB_X2_anal =   (x2-x1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdx2 ;

          
        G_CB_Y1_anal = - (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy1;
        G_CB_Y2_anal =   (y2-y1) / sqrt((x2-x1)^2 +(y2-y1)^2) * F  + sqrt((x2-x1)^2 + (y2-y1)^2)*dFdy2;



        


        end
    end
    G_anal = [G_CB_X1_anal G_CB_X2_anal G_CB_Y1_anal G_CB_Y2_anal];



end

