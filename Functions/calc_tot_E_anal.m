function [Etot] = calc_tot_E_anal(X_cb,Vol0,lambda1,rk1,lambda2,rk2,lambda3,rk3,contact_line_thickness,gamma_1,gamma_2,alpha,grav,rho,SUB_INFO,LU)


W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);

X_cb = convert2_multiarray(X_cb);
% 
% Esurf = calc_surf_E_integral_cutoff_pos( X_cb,contact_line_thickness,gamma_1,gamma_2,alpha,100 ,SUB_INFO);
Esurf = calc_surf_E_anal( X_cb,contact_line_thickness,gamma_1,gamma_2,alpha,SUB_INFO);

[Vol,G_Vol] = calc_volume(X_cb);
h1 = (Vol - Vol0) ;

h2 = calc_h2_one(X_cb,SUB_INFO);

h3 = calc_h3_two(X_cb,alpha,contact_line_thickness,SUB_INFO,LU);

E_LAGRANGE = -lambda1*h1 + rk1/2 * h1^2 - lambda2*h2 + rk2/2 * (h2^2) - lambda3*h3 + rk3/2 * (h3^2);

[ C,GCx,GCy ] = calc_centroid( X_cb );
E_grav = grav*Vol*(C(2) );



% if isinf(Esurf);
%     disp('[warning] Esurf inf');
% end
% if isinf(E_LAGRANGE);
%     disp('[warning] E_LAGRANGE inf');
% end
% if isinf(E_grav);
%     disp('[warning] E_grav inf');
% end
Etot = Esurf + E_LAGRANGE + E_grav ;

end
