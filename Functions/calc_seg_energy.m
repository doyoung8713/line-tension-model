function [ E_anal ] = calc_seg_energy_re( x1,x2,y1,y2,gamma_1,gamma_2,xORy,pORm,contact_line_thickness, alpha)
%UNTITLED2 이 함수의 요약 설명 위치
%   자세한 설명 위치
if pORm == 1; %%% 1 refers +: gamma_1, -: gamma_2, else for opposite
    alpha = alpha;
else
    alpha= -alpha;
end

if xORy == 1; %%% 1 for gamma(x) 

    if ((x1-x2)/(y1-y2))^2<eps
        E_anal = abs(y2-y1) * (1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)*tanh(alpha*(x1-contact_line_thickness))) ;
    else
%         E_anal = sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(x2-x1) * log( ( cosh(alpha*x2 - alpha*(contact_line_thickness))/ cosh(alpha*x1 - alpha*(contact_line_thickness)))));
        E_anal = sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(x2-x1) * (lncosh(alpha*x2 - alpha*(contact_line_thickness)) - lncosh( alpha*x1 - alpha*(contact_line_thickness))));
    end
%     if isinf(E_anal);
%         disp('[warning] xORy=1 inf');
%     end
elseif xORy ==2 %%% 2 for gamma(y)

%      E_anal = sqrt((x2-x1).^2 + (y2-y1).^2) * gamma_1;

    if ((y1-y2)/(x1-x2))^2<eps
%     elseif (y1-y2)^2<eps
        E_anal = abs(x2-x1) * (1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)*tanh(alpha*(y1-contact_line_thickness))) ;
%         E_anal = 0;
    else
%         E_anal = sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(y2-y1) * log( ( cosh(alpha*y2 - alpha*contact_line_thickness)/ cosh(alpha*y1 - alpha*contact_line_thickness))));
        E_anal = sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(y2-y1) * (lncosh(alpha*y2 - alpha*(contact_line_thickness)) - lncosh( alpha*y1 - alpha*(contact_line_thickness))));

    end
    if isinf(E_anal);
        disp('[warning] xORy=2 inf');
        plot([x1 x2],[y1 y2],'b-');
        figure(55);
        
    end
elseif xORy ==3 %%% 3 for gamma(x+y)

    if  ((y1-y2)/(x1-x2)+1 )^2<eps
        E_anal =  sqrt((x2-x1).^2 + (y2-y1).^2) * (1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)*tanh(alpha*(x1+y1-contact_line_thickness))) ;
    else
%         E_anal =  sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(x2+y2-x1-y1) * log( ( cosh(alpha*(x2+y2) - alpha*contact_line_thickness)/ cosh(alpha*(x1+y1) - alpha*contact_line_thickness))));
        E_anal =  sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(x2+y2-x1-y1) * (lncosh(alpha*(x2+y2) - alpha*(contact_line_thickness)) - lncosh( alpha*(x1+y1) - alpha*(contact_line_thickness))));

    end
%     if isinf(E_anal);
%         disp('[warning] xORy=3 inf');
%     end

else %%% else for gamma(-x+y)

    if ((y1-y2)/(x1-x2)-1 )^2<eps
        E_anal =  sqrt((x2-x1).^2 + (y2-y1).^2) * (1/2*(gamma_1+gamma_2) + 1/2 * (gamma_1-gamma_2)*tanh(alpha*(-x1+y1-contact_line_thickness))) ;
    else
%         E_anal =  sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(-x2+y2+x1-y1) * log( ( cosh(alpha*(-x2+y2) - alpha*contact_line_thickness)/ cosh(alpha*(-x1+y1) - alpha*contact_line_thickness))));
        E_anal =  sqrt((x2-x1).^2 + (y2-y1).^2) * ((gamma_1 + gamma_2)/2 + (gamma_1-gamma_2)/2 * 1/alpha/(-x2+y2+x1-y1) * (lncosh(alpha*(-x2+y2) - alpha*(contact_line_thickness)) - lncosh(alpha*(-x1+y1) - alpha*(contact_line_thickness))));

    end
%     if isinf(E_anal);
%         disp('[warning] xORy=4 inf');
%     end
end

end

