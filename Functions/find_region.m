function [ region ] = find_region( x,y,SUB_INFO )
%UNTITLED6 이 함수의 요약 설명 위치
% 1 for a, 2 for b, 3 for c 
W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);


%%% region1

if x<0 || x>(W+G)
%     x;
    
    x
    y
    disp('ERROR [find_region]: Point out of periodicity');
    return
    region = -1;
elseif (y >= -x) && (y >= x - W ) && (x>=0) && (x<W)
    region = 1;
elseif (y < -x) && (x<=W/2) && (x >=0) 
    region = 2;
elseif (y < x-W) && (x>W/2) && (y>=x-W-H) && (x<=W+G/2) && (y<0) 
    region =3;
elseif (x>=W) && (x<=W+G/2) &&(y>=0)
    region = 4;
elseif (y<x-W-H) && (y<=-x+W+G-H) && (x>W/2) && (x<=(W+G))
    region = 5;
elseif (y>-x+W+G-H) && (x>W+G/2) && (y<0) && (x<=W+G)
    region = 6;
elseif (y>=0) && (x>W+G/2) && (x<=W+G)
    region = 7;
else
%     region = 7;
    
    disp('ERROR [find_region]: Point out of region');
    x
    y
    return
     region = 0;
end
%     region = 2;
    
%%% 1. y = -x
%%% 2. y = x-W
%%% 3. x= W/2
end

