function [V,G] = calc_volume(X)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Scripted by "Minsoo Jeong" in Yonsei University
%%% Object : To calculate total energy and force
%%% Input : X - position of cell in 2D array [n X 2] 
%%% Output : 1. V - Volume in 3D, Area in 2D [Scalar]
%%%          2. G - Volume gradient(x1;y1;x2;y2... ) [2n X 1]
%%% Date : Apr.30 2016.  updated on Aug.02.2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X = [X(end,:); X; X(1,:)];

% calculate volume
V = 1/2 * sum((X(2:end-1,1) + X(3:end,1)) .* (X(3:end,2) - X(2:end-1,2)));
% V = 

% calculate gradient
G = 1/2 * [(X(3:end,2) - X(1:end-2,2)), (X(1:end-2,1) - X(3:end,1))];  
% G = convert2_1Darray(G);
end

