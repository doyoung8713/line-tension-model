function [ periods,points,gshape_sorted ] = find_period( x1,x2,y1,y2,SUB_INFO )
%UNTITLED4 이 함수의 요약 설명 위치
%   자세한 설명 위치
W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);


IND_period_1 = floor(x1/(W+G));
IND_period_2 = floor(x2/(W+G));

IND_period_min = min(IND_period_1,IND_period_2);
IND_period_max = max(IND_period_1,IND_period_2);

num_period = abs(IND_period_2 - IND_period_1);


points = [x1 y1 0];
gshape = [1 0 0 0 0 0 1 0];

for q = 1:num_period
    
    if (x2-(W+G)*(IND_period_min+q))*(x1-(W+G)*(IND_period_min+q)) < 0   %%% . x=0

        xc = (W+G)*(IND_period_min+q);
        yc = (y2-y1)/(x2-x1) * (xc-x1) + y1;
        t = sqrt((xc-x1)^2 + (yc-y1)^2)/sqrt((x2-x1)^2 + (y2-y1)^2);

        if (0<t && t<1);
            points(end+1,:) = [xc,yc,t];

            dxp_dx1 = 0;
            dxp_dx2 = 0;
            dxp_dy1 = 0;
            dxp_dy2 = 0;

            dyp_dx1 = -(y2-y1)/(x2-x1) + (y2-y1)*(xc-x1)/(x2-x1)^2;
            dyp_dx2 = -(y2-y1)*(xc-x1)/(x2-x1)^2;
            dyp_dy1 =  (x2-xc)/(x2-x1);
            dyp_dy2 = (xc-x1)/(x2-x1);
            gshape(end+1,:) = [dxp_dx1 dxp_dx2 dxp_dy1 dxp_dy2 dyp_dx1 dyp_dx2 dyp_dy1 dyp_dy2];
        end
    end
end

points(end+1,:) = [x2,y2,1];
gshape(end+1,:)  = [0 1 0 0 0 0 0 1];
[points ind] = sortrows(points,3);
gshape_sorted = gshape(ind,:);


%%%%%%% Periods %%%%%%%%

% N_point = size(points,1);
% IND_period_1
% if IND_period_1< IND_period_2
%     periods = [IND_period_1:1:IND_period_2];
% else
%     periods = [IND_period_1:-1:IND_period_2];
% end
    
    
N_point = size(points,1);

for k =1 : N_point-1;
    mid_point = 1/2 * (points(k,1:2) + points(k+1,1:2));
    periods(k) = floor(mid_point(1)/(W+G));
end
    
    
    







end


