function [ E_line ] = calc_period_energy( x1,x2,y1,y2,gamma_1,gamma_2,contact_line_thickness,alpha,SUB_INFO )
%UNTITLED7 이 함수의 요약 설명 위치
%   자세한 설명 위치
W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);
[ periods,points ] = find_period( x1,x2,y1,y2,SUB_INFO );

N_point = size(points,1);



    for q = 1:N_point-1;
        
        points_period_1 = [points(q,1)-periods(q)*(W+G), points(q,2)];
        points_period_2 = [points(q+1,1)-periods(q)*(W+G), points(q+1,2)];
        
        E_line_data(q) = calc_one_line_energy( points_period_1(1,1),points_period_2(1,1),points_period_1(1,2),points_period_2(1,2),gamma_1,gamma_2,contact_line_thickness, alpha,SUB_INFO);

    end


E_line= sum(E_line_data);
% if isnan(E_line) == 1;
%     figure(55); hold on
%     plot(x1,y1,'ro');
%     x1
%     y1
% end


    







end

