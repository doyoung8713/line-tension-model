function [ E_CB ] = calc_surf_E_anal( CB_coord,contact_line_thickness,gamma_1,gamma_2,alpha,SUB_INFO)
%UNTITLED 이 함수의 요약 설명 위치
%   자세한 설명 위치

N_node = size(CB_coord,1);

flag_data = 0;
for sq = 1:N_node;
    
    if sq == N_node;
        x2 = CB_coord(1,1);
        y2 = CB_coord(1,2);
        x1 = CB_coord(sq,1);
        y1 = CB_coord(sq,2);
    else
    x2 = CB_coord(sq+1,1);   
    y2 = CB_coord(sq+1,2);
    x1 = CB_coord(sq,1);
    y1 = CB_coord(sq,2);
    end
        E_CB_data(sq) = calc_period_energy( x1,x2,y1,y2,gamma_1,gamma_2,contact_line_thickness,alpha,SUB_INFO);
        if isinf(E_CB_data(sq));
            figure(55); hold on
            plot([x1 x2],[y1 y2],'r-');
%             plot(x2,y2,'ro');
            flag_data(sq) = 1;
        end
        

end
if sum(flag_data)>0;
%             disp('[warning] E inf'); 
%             figure(55); hold on
%             plot(CB_coord(:,1),CB_coord(:,2),'b');
%             plot(x2,y2,'bo');
end
    
%     isnan(E_CB_data)
    E_CB = sum(E_CB_data);







end

