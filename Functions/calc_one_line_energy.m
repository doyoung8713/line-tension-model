function [ E_line ] = calc_one_line_energy( x1,x2,y1,y2,gamma_1,gamma_2,contact_line_thickness,alpha,SUB_INFO )
%UNTITLED7 이 함수의 요약 설명 위치
%   자세한 설명 위치

[ regions,points ] = find_points( x1,x2,y1,y2,SUB_INFO );

N_point = size(points,1);

W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);

for q = 1:N_point-1;
    
%      regions(q)   
    if regions(q) == 1;
        E_line_data(q) = calc_seg_energy( points(q,1),points(q+1,1),points(q,2),points(q+1,2),gamma_1,gamma_2,2,1,contact_line_thickness, alpha);
    elseif regions(q) ==2; 
        E_line_data(q) = calc_seg_energy( points(q,1),points(q+1,1),points(q,2),points(q+1,2),gamma_1,gamma_2,1,2,-contact_line_thickness, alpha);
    elseif regions(q) ==3;
        E_line_data(q) = calc_seg_energy( points(q,1),points(q+1,1),points(q,2),points(q+1,2),gamma_1,gamma_2,1,1,W+contact_line_thickness, alpha);
    elseif regions(q) ==4;
        E_line_data(q) = calc_seg_energy( points(q,1),points(q+1,1),points(q,2),points(q+1,2),gamma_1,gamma_2,3,1,W+contact_line_thickness, alpha);
    elseif regions(q) ==5;
        E_line_data(q) = calc_seg_energy( points(q,1),points(q+1,1),points(q,2),points(q+1,2),gamma_1,gamma_2,2,1,-H+contact_line_thickness, alpha);
    elseif regions(q) ==6;
        E_line_data(q) = calc_seg_energy( points(q,1),points(q+1,1),points(q,2),points(q+1,2),gamma_1,gamma_2,1,2,W+G-contact_line_thickness, alpha);
    elseif regions(q) ==7;
        E_line_data(q) = calc_seg_energy( points(q,1),points(q+1,1),points(q,2),points(q+1,2),gamma_1,gamma_2,4,1,-W-G+contact_line_thickness, alpha);
        
            
    end
%     figure(55); hold on
%     plot(points(q,1),points(q,2),'bo'); 
%     if isnan(E_line_data(q)) == 1;
%     figure(55); hold on
%     plot(points(q,1),points(q,2),'ro');
%     points(q,1)
%     points(q,2)
%     regions(q)
%     end
end

E_line= sum(E_line_data);


    







end

