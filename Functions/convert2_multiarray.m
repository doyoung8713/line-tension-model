function ret = convert2_multiarray(X)
% Convert 1D array X to the (np) x (NDIM) matrix X 

NDIM= 2;

ndof = length(X); np = ndof/NDIM;
temp = zeros(np, NDIM); 
for i=1:NDIM 
    temp(:,i) = X(i:NDIM:ndof);
end
ret = temp;

return