function [F,dFdx,dFdy ] = calc_F_segment(  x0,y0,alpha,contact_line_thickness,SUB_INFO)

W = SUB_INFO(1);
G = SUB_INFO(2);
H = SUB_INFO(3);


IND_period = floor(x0/(W+G));
x = x0 - (W+G)*(IND_period);
y = y0;

region = find_region(x,y,SUB_INFO);


    F = 1/2 + 1/2*tanh(alpha*(contact_line_thickness-y));
    dFdx = 0;
    dFdy = 1/2*sech(alpha*(contact_line_thickness-y))^2 * (-alpha);
%     F = 1;
%     dFdx = 0;
%     dFdy = 0;

% if region ==1
%     F = 1/2+1/2*tanh(alpha*(contact_line_thickness-y));
%     dFdx = 0;
%     dFdy = 1/2*sech(alpha*(contact_line_thickness-y))^2 * (-alpha);
% elseif region ==2
%     F = 1/2+1/2*tanh(-alpha*(-contact_line_thickness-x));
%     dFdx = 1/2*sech(-alpha*(-contact_line_thickness-x))^2 * (alpha);
%     dFdy = 0;    
% elseif region ==3
%     F = 1/2+1/2*tanh(alpha*(W+contact_line_thickness-x));
%     dFdx = 1/2*sech(alpha*(W+contact_line_thickness-x))^2 * (-alpha);
%     dFdy = 0;
%     
% elseif region ==4
%     F = 1/2+1/2*tanh(alpha*(W+contact_line_thickness-x-y));
%     dFdx = 1/2*sech(alpha*(W+contact_line_thickness-x-y))^2 * (-alpha);
%     dFdy = 1/2*sech(alpha*(W+contact_line_thickness-x-y))^2 * (-alpha);
% elseif region ==5
%     F = 1/2+1/2*tanh(alpha*(-H+contact_line_thickness-y));
%     dFdx = 0;
%     dFdy = 1/2*sech(alpha*(-H+contact_line_thickness-y))^2 * (-alpha);
% elseif region ==6
%     F = 1/2+1/2*tanh(-alpha*(W+G-contact_line_thickness-x));
%     dFdx = 1/2*sech(-alpha*(W+G-contact_line_thickness-x))^2 * (alpha);
%     dFdy = 0;
% elseif region ==7
%     F =1/2+1/2* tanh(alpha*(-W-G+contact_line_thickness+x-y));
%     dFdx = 1/2*sech(alpha*(-W-G+contact_line_thickness+x-y))^2 * (alpha);
%     dFdy = 1/2*sech(alpha*(-W-G+contact_line_thickness+x-y))^2 * (-alpha);
% end




end

