function [ G_tot_sum_anal] = calc_grad_anal_sum( x1,x2,y1,y2,gamma_1,gamma_2,alpha,contact_line_thickness,SUB_INFO)
%UNTITLED2 이 함수의 요약 설명 위치
%   자세한 설명 위치

    W = SUB_INFO(1);
    G = SUB_INFO(2);
    H = SUB_INFO(3);

%     [x1 x2]
    [ regions,points,gshape ] = find_points( x1,x2,y1,y2 ,SUB_INFO);
    num_region = size(regions,2);

    for k = 1:num_region;
       
        Xa = points(k,1);
        Xb = points(k+1,1);
        Ya = points(k,2);
        Yb = points(k+1,2);

        gshape_x1 = [gshape(k,1) gshape(k+1,1) gshape(k,5) gshape(k+1,5)]';
        gshape_x2 = [gshape(k,2) gshape(k+1,2) gshape(k,6) gshape(k+1,6)]';
        gshape_y1 = [gshape(k,3) gshape(k+1,3) gshape(k,7) gshape(k+1,7)]';
        gshape_y2 = [gshape(k,4) gshape(k+1,4) gshape(k,8) gshape(k+1,8)]';
    

        if regions(k) ==1;
            G_anal = calc_grad_anal( Xa,Xb,Ya,Yb,gamma_1,gamma_2,alpha,contact_line_thickness,2);
        elseif regions(k) ==2;
            G_anal = calc_grad_anal( Xa,Xb,Ya,Yb,gamma_1,gamma_2,-alpha,-contact_line_thickness,1);
        elseif regions(k) ==3;
            G_anal = calc_grad_anal( Xa,Xb,Ya,Yb,gamma_1,gamma_2,alpha,W+contact_line_thickness,1);
        elseif regions(k) ==4;
            G_anal = calc_grad_anal( Xa,Xb,Ya,Yb,gamma_1,gamma_2,alpha,W+contact_line_thickness,3);         
        elseif regions(k) ==5;
            G_anal = calc_grad_anal( Xa,Xb,Ya,Yb,gamma_1,gamma_2,alpha,-H+contact_line_thickness,2);            
        elseif regions(k) ==6;
            G_anal = calc_grad_anal( Xa,Xb,Ya,Yb,gamma_1,gamma_2,-alpha,W+G-contact_line_thickness,1);            
        elseif regions(k) ==7;
            G_anal = calc_grad_anal( Xa,Xb,Ya,Yb,gamma_1,gamma_2,alpha,-W-G+contact_line_thickness,4);            
          
        end

        G_tot_anal(k,:) = [G_anal * gshape_x1 G_anal * gshape_x2  G_anal * gshape_y1  G_anal * gshape_y2];
%         G_tot_anal(k,:) = G_anal;
        
    end

        G_tot_sum_anal = sum(G_tot_anal,1);







    end


