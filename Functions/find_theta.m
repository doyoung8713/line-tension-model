function [ theta_c, R_c, res_c] = find_theta( L,R0, Maxiter, res_i )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

L_th = @(theta)  2* ((pi*R0.^2./(theta-sin(theta).*cos(theta))).^0.5) .* sin(theta);

a_theta_c = pi/100000;
b_theta_c = pi-a_theta_c;

for i = 1:Maxiter;
    res = (L-L_th((a_theta_c + b_theta_c)/2))/L;
    if abs(res) > res_i 
    
        if res > 0;
            a_theta_c = a_theta_c;
            b_theta_c = (a_theta_c + b_theta_c)/2;
        else
            a_theta_c = (a_theta_c + b_theta_c)/2;
            b_theta_c = b_theta_c;
        end
    else
        break
    end
end

theta_c = (a_theta_c + b_theta_c)/2;
R_c = ((pi*R0.^2./(theta_c-sin(theta_c).*cos(theta_c))).^0.5);
res_c = (L-L_th(theta_c))/L;



end

